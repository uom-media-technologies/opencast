# manchester-opencast.spec
# Package Manchester's Opencast

%define     _product_name manchester-opencast
%define     _prefix /opt/opencast
%define     _data_prefix /var/opencast
%define     __jar_repack 0

Name:        %{_product_name}

# The following lines are edited by the rpm-build.py script
Version:    CHANGE_ME_VERSION
Release:    CHANGE_ME_RELEASE
Summary:    Manchester Opencast
License:    ECL 2.0
URL:        http://itservices.manchester.ac.uk
BuildRoot:  %{_tmppath}/%{_product_name}-%{version}-%{release}
Source:     %{_product_name}-%{version}-%{release}.tar.gz

# Baseline requirements
Requires:   java-11

%global debug_package %{nil}

%description
Manchester Opencast video management and processing system

%prep
%setup -n %{_product_name}-%{version}-%{release}

%build

%pre
#first install
if [ "$1" = "1" ]; then
  getent group opencast >/dev/null || groupadd opencast
  getent passwd opencast >/dev/null || useradd -d /opt/opencast -m -g opencast opencast -r -s /sbin/nologin -c "Opencast media processing"
  install -d -m 755 $RPM_BUILD_ROOT/var/log/opencast

#things to do before an update
elif [ "$1" = "2" ]; then
  #stop opencast
  systemctl stop opencast >/dev/null 2>&1
  #clear caches
  rm -rf /var/cache/opencast/* /var/tmp/opencast/* >/dev/null 2>&1
fi

%install
# Work directories
mkdir -p $RPM_BUILD_ROOT/var/cache/opencast
mkdir -p $RPM_BUILD_ROOT/var/tmp/opencast
mkdir -p $RPM_BUILD_ROOT/var/log/opencast

# Data directories
mkdir -p $RPM_BUILD_ROOT%{_data_prefix}/distribution/downloads
mkdir -p $RPM_BUILD_ROOT%{_data_prefix}/distribution/streams
mkdir -p $RPM_BUILD_ROOT%{_data_prefix}/archive
mkdir -p $RPM_BUILD_ROOT%{_data_prefix}/work/inbox
mkdir -p $RPM_BUILD_ROOT%{_data_prefix}/work/shared/workspace
mkdir -p $RPM_BUILD_ROOT%{_data_prefix}/work/shared/files

# Install Opencast
install -d -m 755 $RPM_BUILD_ROOT%{_prefix}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

# Opencast docs
install -D -m 644 LICENSE $RPM_BUILD_ROOT%{_prefix}/LICENSE
install -D -m 644 NOTICES $RPM_BUILD_ROOT%{_prefix}/NOTICES
install -D -m 644 README.md $RPM_BUILD_ROOT%{_prefix}/README.md

# Opencast systemd scripts
install -D -m 755 bin/setenv $RPM_BUILD_ROOT%{_prefix}/bin/setenv
install -D -m 755 bin/check_ports $RPM_BUILD_ROOT%{_prefix}/bin/check_ports
install -D -m 755 bin/inc $RPM_BUILD_ROOT%{_prefix}/bin/inc
install -D -m 755 bin/start-opencast $RPM_BUILD_ROOT%{_prefix}/bin/start-opencast
install -D -m 755 bin/stop-opencast $RPM_BUILD_ROOT%{_prefix}/bin/stop-opencast
install -D -m 644 docs/service/opencast.service $RPM_BUILD_ROOT%{_unitdir}/opencast.service

# Use cp for 'hot' files
# Install the Opencast libraries an configuration
cp -rf lib $RPM_BUILD_ROOT%{_prefix}
cp -rf system $RPM_BUILD_ROOT%{_prefix}
cp -rf etc $RPM_BUILD_ROOT%{_prefix}

%post
# First install only
if [ "$1" =  "1" ]; then
  ln -s %{_prefix}/etc ${RPM_BUILD_ROOT}/etc/opencast
fi

%clean
rm -rf %{buildroot}

%files
# These files must match what the the rpm-build script generates
%defattr(0644,opencast,opencast,0755)
%dir %{_prefix}
%license %{_prefix}/LICENSE
%doc %{_prefix}/NOTICES
%doc %{_prefix}/README.md
%config(noreplace) %{_prefix}/bin/setenv
%attr(0755,opencast,opencast) %{_prefix}/bin
%config(noreplace) %{_prefix}/etc
%{_prefix}/lib
%{_prefix}/system

%attr(0755,opencast,opencast) /var/cache/opencast
%attr(0755,opencast,opencast) /var/tmp/opencast
%attr(0755,opencast,opencast) /var/log/opencast
%config(noreplace) %attr(0644,root,root) %{_unitdir}/opencast.service

%preun
if [ "$1" = "0" ]; then
  systemctl stop opencast
fi

%postun
if [ "$1" = "0" ]; then
  systemctl disable opencast
  rm -f  /etc/opencast
  rm -rf /var/cache/opencast
  rm -rf /var/log/opencast
  rm -rf /var/tmp/opencast
fi

%changelog
CHANGE_ME_CHANGELOG
