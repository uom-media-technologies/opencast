-- MAT-570, can't handle lists of emails
ALTER TABLE oc_assets_properties
    MODIFY COLUMN val_string VARCHAR(512);