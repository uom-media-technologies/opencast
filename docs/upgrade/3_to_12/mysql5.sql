-- Update Particapation Management tables from 3.x to 12.x
-- #######################################################

-- MESSAGE SERVICE

-- Duplicate 'pm_' tables existed in old schema for some reason
DROP TABLE IF EXISTS mh_pm_message_template;
DROP TABLE IF EXISTS mh_pm_message_signature;

-- NOTE: there are some indexes prepended with FK_ presumably these were autocreated
--       when the Foriegn Keys were added and then exported in the schema. Rename these
--       with IX_ for clarity

-- Participation Tables
-- scheduling source
RENAME TABLE mh_pm_scheduling_source TO oc_pm_scheduling_source;
ALTER TABLE oc_pm_scheduling_source DROP INDEX IF EXISTS UNQ_mh_pm_scheduling_source_0;
ALTER TABLE oc_pm_scheduling_source ADD CONSTRAINT UNQ_oc_pm_scheduling_source_0 UNIQUE (source);

-- action
RENAME TABLE mh_pm_action TO oc_pm_action;

-- blacklist
RENAME TABLE mh_pm_blacklist TO oc_pm_blacklist;
RENAME TABLE mh_pm_period TO oc_pm_period;
RENAME TABLE mh_pm_blacklist_mh_pm_period TO oc_pm_blacklist_oc_pm_period;

CREATE INDEX IX_oc_pm_blacklist_oc_pm_period_periods_id ON oc_pm_blacklist_oc_pm_period (periods_id);
DROP INDEX IF EXISTS FK_mh_pm_blacklist_mh_pm_period_periods_id ON oc_pm_blacklist_oc_pm_period;

ALTER TABLE oc_pm_blacklist_oc_pm_period ADD CONSTRAINT `FK_oc_pm_blacklist_oc_pm_period_blacklist_id` FOREIGN KEY (`Blacklist_id`) REFERENCES `oc_pm_blacklist` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_blacklist_oc_pm_period DROP FOREIGN KEY FK_mh_pm_blacklist_mh_pm_period_Blacklist_id;
ALTER TABLE oc_pm_blacklist_oc_pm_period ADD CONSTRAINT `FK_oc_pm_blacklist_oc_pm_period_periods_id` FOREIGN KEY (`periods_id`) REFERENCES `oc_pm_period` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_blacklist_oc_pm_period DROP FOREIGN KEY FK_mh_pm_blacklist_mh_pm_period_periods_id;

-- building
RENAME TABLE mh_pm_building TO oc_pm_building;
RENAME TABLE mh_pm_room TO oc_pm_room;
RENAME TABLE mh_pm_building_mh_pm_room TO oc_pm_building_oc_pm_room;
ALTER TABLE oc_pm_building ADD CONSTRAINT UNQ_oc_pm_building_0 UNIQUE (name);
DROP INDEX IF EXISTS UNQ_mh_pm_building_0 ON oc_pm_building;
--   duplicate index
DROP INDEX IF EXISTS IX_mh_pm_building_name ON oc_pm_building;
ALTER TABLE oc_pm_room ADD CONSTRAINT UNQ_oc_pm_room_0 UNIQUE (name);
DROP INDEX IF EXISTS UNQ_mh_pm_room_0 ON oc_pm_room ;
CREATE INDEX FK_oc_pm_building_oc_pm_room_rooms_id ON oc_pm_building_oc_pm_room (rooms_id);
DROP INDEX IF EXISTS FK_mh_pm_building_mh_pm_room_rooms_id ON oc_pm_building_oc_pm_room;

ALTER TABLE oc_pm_building_oc_pm_room ADD CONSTRAINT `FK_oc_pm_building_oc_pm_room_building_id` FOREIGN KEY (`Building_id`) REFERENCES `oc_pm_building` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_building_oc_pm_room DROP FOREIGN KEY FK_mh_pm_building_mh_pm_room_Building_id;
ALTER TABLE oc_pm_building_oc_pm_room ADD CONSTRAINT `FK_oc_pm_building_oc_pm_room_rooms_id` FOREIGN KEY (`rooms_id`) REFERENCES `oc_pm_room` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_building_oc_pm_room DROP FOREIGN KEY FK_mh_pm_building_mh_pm_room_rooms_id;

-- capture agent
RENAME TABLE mh_pm_capture_agent TO oc_pm_capture_agent;
ALTER TABLE oc_pm_capture_agent CHANGE COLUMN `mh_agent` oc_agent VARCHAR(255);
DROP INDEX IF EXISTS UNQ_mh_pm_capture_agent_0 ON oc_pm_capture_agent;
ALTER TABLE oc_pm_capture_agent ADD CONSTRAINT UNQ_oc_pm_capture_agent_0 UNIQUE (oc_agent);
--   duplicate index
DROP INDEX IF EXISTS IX_mh_pm_capture_agent_agent ON oc_pm_capture_agent;
CREATE INDEX FK_oc_pm_capture_agent_room ON oc_pm_capture_agent (room);
DROP INDEX IF EXISTS FK_mh_pm_capture_agent_room ON oc_pm_capture_agent;

ALTER TABLE oc_pm_capture_agent ADD CONSTRAINT `FK_oc_pm_capture_agent_room` FOREIGN KEY (`room`) REFERENCES `oc_pm_room` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_capture_agent DROP FOREIGN KEY FK_mh_pm_capture_agent_room;

-- course
RENAME TABLE mh_pm_course TO oc_pm_course;
ALTER TABLE oc_pm_course ADD CONSTRAINT UNQ_oc_pm_course_0 UNIQUE (course);
DROP INDEX IF EXISTS UNQ_mh_pm_course_0 ON oc_pm_course;
CREATE INDEX FK_oc_pm_course_scheduling_source ON oc_pm_course (source);
DROP INDEX IF EXISTS FK_mh_pm_course_scheduling_source ON oc_pm_course;
CREATE INDEX IX_oc_pm_course_course ON oc_pm_course (course);
DROP INDEX IF EXISTS IX_mh_pm_course_course ON oc_pm_course;
CREATE INDEX IX_oc_pm_course_series ON oc_pm_course (series);
DROP INDEX IF EXISTS IX_mh_pm_course_series ON oc_pm_course;
CREATE INDEX IX_oc_pm_course_opted_out ON oc_pm_course (opted_out);
DROP INDEX IF EXISTS IX_mh_pm_course_opted_out ON oc_pm_course;

ALTER TABLE oc_pm_course ADD CONSTRAINT `FK_oc_pm_course_scheduling_source` FOREIGN KEY (`source`) REFERENCES `oc_pm_scheduling_source` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_course DROP FOREIGN KEY FK_mh_pm_course_scheduling_source;

-- person
RENAME TABLE mh_pm_person TO oc_pm_person;
RENAME TABLE mh_pm_person_type TO oc_pm_person_type;
RENAME TABLE mh_pm_person_mh_pm_person_type TO oc_pm_person_oc_pm_person_type;

ALTER TABLE oc_pm_person ADD CONSTRAINT UNQ_oc_pm_person_0 UNIQUE (email);
DROP INDEX IF EXISTS UNQ_mh_pm_person_0 ON oc_pm_person;
--   duplicate index
DROP INDEX IF EXISTS IX_mh_pm_person_email ON oc_pm_person;

ALTER TABLE oc_pm_person_type ADD CONSTRAINT UNQ_oc_pm_person_type_0 UNIQUE (name);
DROP INDEX IF EXISTS UNQ_mh_pm_person_type_0 ON oc_pm_person_type;
--   duplicate index
DROP INDEX IF EXISTS IX_mh_pm_person_type_name ON oc_pm_person_type;

CREATE INDEX FK_oc_pm_person_oc_pm_person_type_personTypes_id ON oc_pm_person_oc_pm_person_type (personTypes_id);
DROP INDEX IF EXISTS FK_mh_pm_person_mh_pm_person_type_personTypes_id ON oc_pm_person_oc_pm_person_type;

ALTER TABLE oc_pm_person_oc_pm_person_type ADD CONSTRAINT `FK_oc_pm_person_oc_pm_person_type_person_id` FOREIGN KEY (`Person_id`) REFERENCES `oc_pm_person` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_person_oc_pm_person_type ADD CONSTRAINT `FK_oc_pm_person_oc_pm_person_type_persontypes_id` FOREIGN KEY (`personTypes_id`) REFERENCES `oc_pm_person_type` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_person_oc_pm_person_type DROP FOREIGN KEY FK_mh_pm_person_mh_pm_person_type_Person_id;
ALTER TABLE oc_pm_person_oc_pm_person_type DROP FOREIGN KEY FK_mh_pm_person_mh_pm_person_type_personTypes_id;

-- messages & errors
RENAME TABLE mh_pm_error TO oc_pm_error;
RENAME TABLE mh_pm_message TO oc_pm_message;
RENAME TABLE mh_pm_message_mh_pm_error TO oc_pm_message_oc_pm_error;

CREATE INDEX FK_oc_pm_message_template ON oc_pm_message (template);
DROP INDEX IF EXISTS FK_mh_pm_message_template ON oc_pm_message;
CREATE INDEX FK_oc_pm_message_creator ON oc_pm_message (creator);
DROP INDEX IF EXISTS FK_mh_pm_message_creator ON oc_pm_message;
CREATE INDEX FK_oc_message_signature ON oc_pm_message (signature);
DROP INDEX IF EXISTS FK_mh_message_signature ON oc_pm_message;
CREATE INDEX IX_oc_pm_message_creation_date ON oc_pm_message (creation_date);
DROP INDEX IF EXISTS IX_mh_pm_message_creation_date ON oc_pm_message;
CREATE INDEX FK_oc_pm_message_oc_pm_error_errors_id ON oc_pm_message_oc_pm_error (errors_id);
DROP INDEX IF EXISTS FK_mh_pm_message_mh_pm_error_errors_id ON oc_pm_message_oc_pm_error;

ALTER TABLE oc_pm_message ADD CONSTRAINT `FK_oc_message_signature` FOREIGN KEY (`signature`) REFERENCES `oc_message_signature` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_message ADD CONSTRAINT `FK_oc_message_template` FOREIGN KEY (`template`) REFERENCES `oc_message_template` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_message ADD CONSTRAINT `FK_oc_pm_message_creator` FOREIGN KEY (`creator`) REFERENCES `oc_pm_person` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_message DROP FOREIGN KEY FK_mh_message_signature;
ALTER TABLE oc_pm_message DROP FOREIGN KEY FK_mh_message_template;
ALTER TABLE oc_pm_message DROP FOREIGN KEY FK_mh_pm_message_creator;

ALTER TABLE oc_pm_message_oc_pm_error ADD CONSTRAINT `FK_oc_pm_message_oc_pm_error_message_id` FOREIGN KEY (`Message_id`) REFERENCES `oc_pm_message` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_message_oc_pm_error ADD CONSTRAINT `FK_oc_pm_message_oc_pm_error_errors_id` FOREIGN KEY (`errors_id`) REFERENCES `oc_pm_error` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_message_oc_pm_error DROP FOREIGN KEY FK_mh_pm_message_mh_pm_error_Message_id;
ALTER TABLE oc_pm_message_oc_pm_error DROP FOREIGN KEY FK_mh_pm_message_mh_pm_error_errors_id;

-- recording
RENAME TABLE mh_pm_recording TO oc_pm_recording;
ALTER TABLE oc_pm_recording CHANGE COLUMN event_id mediapackage VARCHAR(255);
RENAME TABLE mh_pm_recording_action TO oc_pm_recording_action;
RENAME TABLE mh_pm_recording_messages TO oc_pm_recording_messages;
RENAME TABLE mh_pm_recording_participation TO oc_pm_recording_participation;
RENAME TABLE mh_pm_recording_staff TO oc_pm_recording_staff;
ALTER TABLE oc_pm_recording ADD CONSTRAINT UNQ_oc_pm_recording_0 UNIQUE (activity,start_date,end_date,room);
DROP INDEX IF EXISTS UNQ_mh_pm_recording_0 ON oc_pm_recording;
CREATE INDEX FK_oc_pm_recording_course ON oc_pm_recording (course);
DROP INDEX IF EXISTS FK_mh_pm_recording_course ON oc_pm_recording;
CREATE INDEX FK_oc_pm_recording_room ON oc_pm_recording (room);
DROP INDEX IF EXISTS FK_mh_pm_recording_room ON oc_pm_recording;
-- duplicate
DROP INDEX IF EXISTS IX_mh_pm_recording_room ON oc_pm_recording;
CREATE INDEX FK_oc_pm_recording_capture_agent ON oc_pm_recording (capture_agent);
DROP INDEX IF EXISTS FK_mh_pm_recording_capture_agent ON oc_pm_recording;
CREATE INDEX FK_oc_pm_recording_source ON oc_pm_recording (source);
DROP INDEX IF EXISTS FK_mh_pm_recording_scheduling_source ON oc_pm_recording;
CREATE INDEX IX_oc_pm_recording_deleted ON oc_pm_recording (deleted);
DROP INDEX IF EXISTS IX_mh_pm_recording_deleted ON oc_pm_recording;
CREATE INDEX IX_oc_pm_recording_start_date ON oc_pm_recording (start_date);
DROP INDEX IF EXISTS IX_mh_pm_recording_start_date ON oc_pm_recording;
CREATE INDEX IX_oc_pm_recording_end_date ON oc_pm_recording (end_date);
DROP INDEX IF EXISTS IX_mh_pm_recording_end_date ON oc_pm_recording;
CREATE INDEX IX_oc_pm_recording_review_status ON oc_pm_recording (REVIEWSTATUS);
DROP INDEX IF EXISTS IX_mh_pm_recording_review_status ON oc_pm_recording;
CREATE INDEX IX_oc_pm_recording_email_status ON oc_pm_recording (EMAILSTATUS);
DROP INDEX IF EXISTS IX_mh_pm_recording_email_status ON oc_pm_recording;
CREATE INDEX IX_oc_pm_recording_review_date ON oc_pm_recording (review_date);
DROP INDEX IF EXISTS IX_mh_pm_recording_review_date ON oc_pm_recording;
--    unused
DROP INDEX IF EXISTS IX_mh_pm_recording_blacklisted ON oc_pm_recording;

CREATE INDEX FK_oc_pm_recording_action_action_id ON oc_pm_recording_action (action_id);
DROP INDEX IF EXISTS FK_mh_pm_recording_action_action_id ON oc_pm_recording_action;

CREATE INDEX FK_oc_pm_recording_messages_message_id ON oc_pm_recording_messages (message_id);
DROP INDEX IF EXISTS FK_mh_pm_recording_messages_message_id ON oc_pm_recording_messages;

CREATE INDEX FK_oc_pm_recording_participation_person_id ON oc_pm_recording_participation (person_id);
DROP INDEX IF EXISTS FK_mh_pm_recording_participation_person_id ON oc_pm_recording_participation;

CREATE INDEX FK_oc_pm_recording_staff_person_id ON oc_pm_recording_staff (person_id);
DROP INDEX IF EXISTS FK_mh_pm_recording_staff_person_id ON oc_pm_recording_staff;

ALTER TABLE oc_pm_recording ADD CONSTRAINT `FK_oc_pm_recording_capture_agent` FOREIGN KEY (`capture_agent`) REFERENCES `oc_pm_capture_agent` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording ADD CONSTRAINT `FK_oc_pm_recording_course` FOREIGN KEY (`course`) REFERENCES `oc_pm_course` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording ADD CONSTRAINT `FK_oc_pm_recording_room` FOREIGN KEY (`room`) REFERENCES `oc_pm_room` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording ADD CONSTRAINT `FK_oc_pm_recording_scheduling_source` FOREIGN KEY (`source`) REFERENCES `oc_pm_scheduling_source` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording DROP FOREIGN KEY FK_mh_pm_recording_capture_agent;
ALTER TABLE oc_pm_recording DROP FOREIGN KEY FK_mh_pm_recording_course;
ALTER TABLE oc_pm_recording DROP FOREIGN KEY FK_mh_pm_recording_room;
ALTER TABLE oc_pm_recording DROP FOREIGN KEY FK_mh_pm_recording_scheduling_source;

ALTER TABLE oc_pm_recording_action ADD CONSTRAINT `FK_oc_pm_recording_action_action_id` FOREIGN KEY (`action_id`) REFERENCES `oc_pm_action` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_action ADD CONSTRAINT `FK_oc_pm_recording_action_recording_id` FOREIGN KEY (`recording_id`) REFERENCES `oc_pm_recording` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_action DROP FOREIGN KEY FK_mh_pm_recording_action_action_id;
ALTER TABLE oc_pm_recording_action DROP FOREIGN KEY FK_mh_pm_recording_action_recording_id;

ALTER TABLE oc_pm_recording_messages ADD CONSTRAINT `FK_oc_pm_recording_messages_message_id` FOREIGN KEY (`message_id`) REFERENCES `oc_pm_message` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_messages ADD CONSTRAINT `FK_oc_pm_recording_messages_recording_id` FOREIGN KEY (`recording_id`) REFERENCES `oc_pm_recording` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_messages DROP FOREIGN KEY FK_mh_pm_recording_messages_message_id;
ALTER TABLE oc_pm_recording_messages DROP FOREIGN KEY FK_mh_pm_recording_messages_recording_id;

ALTER TABLE oc_pm_recording_participation ADD CONSTRAINT `FK_oc_pm_recording_participation_person_id` FOREIGN KEY (`person_id`) REFERENCES `oc_pm_person` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_participation ADD CONSTRAINT `FK_oc_pm_recording_participation_recording_id` FOREIGN KEY (`recording_id`) REFERENCES `oc_pm_recording` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_participation DROP FOREIGN KEY FK_mh_pm_recording_participation_person_id;
ALTER TABLE oc_pm_recording_participation DROP FOREIGN KEY FK_mh_pm_recording_participation_recording_id;

ALTER TABLE oc_pm_recording_staff ADD CONSTRAINT `FK_oc_pm_recording_staff_person_id` FOREIGN KEY (`person_id`) REFERENCES `oc_pm_person` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_staff ADD CONSTRAINT `FK_oc_pm_recording_staff_recording_id` FOREIGN KEY (`recording_id`) REFERENCES `oc_pm_recording` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_recording_staff DROP FOREIGN KEY FK_mh_pm_recording_staff_person_id;
ALTER TABLE oc_pm_recording_staff DROP FOREIGN KEY FK_mh_pm_recording_staff_recording_id;

-- synchronization
RENAME TABLE mh_pm_synchronization TO oc_pm_synchronization;
RENAME TABLE mh_pm_synchronized_recording TO oc_pm_synchronized_recording;
RENAME TABLE mh_pm_synchronization_mh_pm_error TO oc_pm_synchronization_oc_pm_error;
RENAME TABLE mh_pm_synchronization_mh_pm_synchronized_recording TO oc_pm_synchronization_oc_pm_synchronized_recording;

ALTER TABLE oc_pm_synchronization DROP INDEX IF EXISTS UNQ_mh_pm_synchronization_0;
ALTER TABLE oc_pm_synchronization ADD CONSTRAINT UNQ_oc_pm_synchronization_0 UNIQUE (date);
--    duplicate index
DROP INDEX IF EXISTS IX_mh_pm_synchronization_date ON oc_pm_synchronization;

CREATE INDEX FK_oc_pm_synchronized_recording_recording ON oc_pm_synchronized_recording (recording);
DROP INDEX IF EXISTS FK_mh_pm_synchronized_recording_recording ON oc_pm_synchronized_recording;

CREATE INDEX IX_oc_pm_syncn_rec_oc_pm_sync_rec_sync_rec_id ON oc_pm_synchronization_oc_pm_synchronized_recording (synchronizedRecordings_id);
DROP INDEX IF EXISTS mhpmsynchrnztnmhpmsynchrnzdrcrdngsynchrnzdRcrdngsd ON oc_pm_synchronization_oc_pm_synchronized_recording;

CREATE INDEX FK_oc_pm_synchronization_oc_pm_error_errors_id ON oc_pm_synchronization_oc_pm_error (errors_id);
DROP INDEX IF EXISTS FK_mh_pm_synchronization_mh_pm_error_errors_id ON oc_pm_synchronization_oc_pm_error;

ALTER TABLE oc_pm_synchronized_recording ADD CONSTRAINT `FK_oc_pm_synchronized_recording_recording` FOREIGN KEY (`recording`) REFERENCES `oc_pm_recording` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_synchronized_recording DROP FOREIGN KEY FK_mh_pm_synchronized_recording_recording;

ALTER TABLE oc_pm_synchronization_oc_pm_error ADD CONSTRAINT `FK_oc_pm_synchronization_oc_pm_error_errors_id` FOREIGN KEY (`errors_id`) REFERENCES `oc_pm_error` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_synchronization_oc_pm_error ADD CONSTRAINT `FK_oc_pm_syncn_oc_pm_error_synchronization_id` FOREIGN KEY (`Synchronization_id`) REFERENCES `oc_pm_synchronization` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_synchronization_oc_pm_error DROP FOREIGN KEY FK_mh_pm_synchronization_mh_pm_error_errors_id;
ALTER TABLE oc_pm_synchronization_oc_pm_error DROP FOREIGN KEY mhpm_synchronization_mh_pm_errorSynchronization_id;

ALTER TABLE oc_pm_synchronization_oc_pm_synchronized_recording ADD CONSTRAINT `FK_oc_pm_syncn_rec_oc_pm_sync_rec_sync_rec_id` FOREIGN KEY (`synchronizedRecordings_id`) REFERENCES `oc_pm_synchronized_recording` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_synchronization_oc_pm_synchronized_recording ADD CONSTRAINT `FK_oc_pm_syncn_rec_oc_pm_sync_rec_sync _id` FOREIGN KEY (`Synchronization_id`) REFERENCES `oc_pm_synchronization` (`id`) ON DELETE CASCADE;
ALTER TABLE oc_pm_synchronization_oc_pm_synchronized_recording DROP FOREIGN KEY mhpmsynchrnztnmhpmsynchrnzdrcrdngsynchrnzdRcrdngsd;
ALTER TABLE oc_pm_synchronization_oc_pm_synchronized_recording DROP FOREIGN KEY mhpmsynchrnztnmhpmsynchronizedrecordingSynchrnztnd;

-- update all future scheduled recordings to use the mediapackage id
UPDATE oc_pm_recording as r
  SET r.mediapackage = (SELECT se.`mediapackage_id` FROM `mh_scheduled_event` as se where r.`mediapackage` = se.`id`)
where r.start_date > NOW() and r.`deleted` = 0;