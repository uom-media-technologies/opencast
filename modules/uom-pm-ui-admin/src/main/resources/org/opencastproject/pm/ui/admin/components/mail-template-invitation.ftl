<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<div dir="ltr">
		<head>
			<style>
				body {
				font-family: "Helvetica", sans-serif;
				color: #49477C;
				font-size: 14px;
				}

				<!-- Client-specific Styles -->#outlook a {
				padding: 0;
				}

				<!-- Force Outlook to provide a "view in browser" menu link. -->body {
				width: 100% !important;
				-webkit-text-size-adjust: 100%;
				-ms-text-size-adjust: 100%;
				margin: 0;
				padding: 0;
				}

				<!-- Prevent Webkit and Windows Mobile platforms from changing default font sizes,
				while not breaking desktop design. -->.ExternalClass {
				width: 100%;
				}

				<!--Force Hotmail to display emails at full width -->.ExternalClass,
				.ExternalClass p,
				.ExternalClass span,
				.ExternalClass font,
				.ExternalClass td,
				.ExternalClass div {
				line-height: 100%;
				}

				<!-- Force Hotmail to display normal line spacing.-->@media only screen and (min-width:480px) {
				.mj-column-per-100 {
				width: 100% !important;
				max-width: 100%;
				}
				.mj-column-per-33 {
				width: 33.33333333333333% !important;
				max-width: 33.33333333333333%;
				}
				}

				@media only screen and (max-width:480px) {
				table.full-width-mobile {
				width: 100% !important;
				}
				td.full-width-mobile {
				width: auto !important;
				}
				}
			</style>

			<!--[if (mso)|(mso 16)]>
				<style type="text/css">
					a {text-decoration: none;}
				</style>
			<![endif]-->
			<!--[if (gte mso 9)|(IE)]>
				<style type="text/css">
					a{ 
						font-weight: normal !important;
						text-decoration: none !important;
					}
			</style>
			<![endif]-->
		<Title>Manage your Podcasting recording preferences</Title>

		</head>

		<body>
			<div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">Review and manage your podcasting recording preferences</div>
			<center>
				<div style="width:642;text-align:center;font-size:12px;color:#666666;margin-top:0;margin-bottom:4px;font-family:Helvetica,sans-serif;font-weight:normal;line-height:normal;">Having trouble? View the <a href="https://www.mypodcasts.manchester.ac.uk/manage-your-recording-preferences/">online version</a></div>
				<table role="presentation" aria-hidden="true" width="642" class="MsoTableGrid" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;">
					<tbody>
						<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes">
							<td width="642" valign="top" style="width:642;">							
								<table role="presentation" aria-hidden="true" class="MsoTableGrid" border="0" cellspacing="0" cellpadding="0" width="642" style="border-collapse:collapse;border:none;width:642px;background-color:#e7e7e7;">
									<tr width="642" style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td valign="middle" align="center" style="text-align:center;margin-top:auto;margin-bottom:auto;"><p style="color:#49447c;font-size:2em;font-weight:normal;display:block;text-align:center;mso-line-height-rule:exactly;line-height:150%;">Podcasting</p>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
								<table role="presentation" aria-hidden="true" width="642">
									<tbody>
										<tr style="mso-yfti-irow:1">
											<td width="642" valign="top" style="width:642px;background:white;">
												<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;mso-line-height-rule:exactly;line-height:normal"><span style=";mso-no-proof:
														yes">&nbsp;</span></p>
											</td>
										</tr>
										<tr style="mso-yfti-irow:2;mso-yfti-lastrow:yes">
											<td valign="top" style="background:white;padding:0cm;">
												<table aria-hidden="false" class="MsoTableGrid" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none">
													<tbody>
														<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
															<td valign="top" style="padding-bottom:8px; text-align:justify;">
																<p class="MsoNormal" style="mso-line-height-rule:exactly;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c">You are receiving this email because you are associated with
																		courses in the timetable which are scheduled to be recorded by
																		the Podcasting system.</span></p>
																<p class="MsoNormal" style="mso-line-height-rule:exactly;line-height:normal"><span style="font-size:12.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c">Please review the information below and set your
																		recording preferences accordingly.</span></p>
															</td>
														</tr>
													</tbody>
												</table>
											<center>
												<table width="642" role="presentation" aria-hidden="true" class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none">
													<tbody>
														<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes" align="center">
															<td width="642" style="width:642.0px;border:none;background:white;text-align:center;" align="center">
																<p class="MsoNormal" style="margin-bottom:0cm;mso-line-height-rule:exactly;line-height:normal">&nbsp;</p>
																	<table aria-hidden="false" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-line-height-rule:exactly;line-height:100%;" caption="Recording preferences button">
																		<tbody>
																			<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes">
																				<td align="center" valign="middle" bgcolor="#F56C54" style="border:none;border-radius:10px;padding:10px 25px"><a href="${optOutLink}" style="text-decoration:none !important; background:#F56C54;color:#fffffe;font-family:Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:120%;Margin:0;text-decoration:none;text-transform:none;letter-spacing:0.5px;display:inline-block;"
																							target="_blank" alt="link to managing recording preferences page">
																						Set Your Recording Preferences</a> 
																				</td>
																			</tr>
																		</tbody>
																	</table>
																	<p aria-hidden="true" class="MsoNormal" style="margin:0px;mso-line-height-rule:exactly;line-height:150%">&nbsp;</p>
															</td>
														</tr>
													</tbody>
												</table>
											</center>
												<table width="642" aria-hidden="false" caption="DASS requirement or Camera available alerts">
													<tbody>
														<#list modules as module>
														<#if module.required>
															<tr align="center">
																<td width="642" valign="middle" cellpadding="6" style="color: #004085;background-color: #cce5ff; padding:6; margin-bottom: 6; border: 1px solid #b8daff; border-radius: .25rem; text-align:left; mso-line-height-rule:exactly; line-height:15.5pt;">
																	<p class="MsoNormal"><span style="font-family:&quot;Helvetica&quot;,sans-serif;font-size:14px;"><strong>DASS requirement</strong> - one or more of your courses have DASS students who require podcasts as part of their support plans, therefore opted-out podcasts will still be recorded. [<a style="text-decoration: none;" href="https://www.mypodcasts.manchester.ac.uk/staff-faqs#dass_staff">Further information</a>]</span></p>
																</td>
															</tr>
															<#break>
															</#if>
															</#list>
															<tr>
																<td style="padding-bottom:6px;"></td>
															</tr>
															<#list modules as module>
															<#if module.cameraAvailable>	
																<tr align="center">
																	<td width="642" valign="middle" style="color: #155724; background-color: #d4edda; padding:6; margin-bottom: 6; border: 1px solid #c3e6cb; border-radius: .25rem; text-align:left;mso-line-height-rule:exactly; line-height:15.5pt;">
																		<p class="MsoNormal"><span style="font-family:&quot;Helvetica&quot;,sans-serif;font-size:14px;"><strong>Camera available</strong> - You are teaching in a room which has a video camera installed so you can opt in to recording video as part of your podcasts. [<a style="text-decoration:none;" href="http://mypodcasts.manchester.ac.uk/cameras/">Further information</a>]</span></p>
																	</td>
																</tr>
															<#break>
															</#if>
															</#list>
															<tr>
																<td style="padding-bottom:8px;"></td>
															</tr>
													</tbody>
												</table>
												<table width="642" class="MsoTableGrid" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border:none;">
													<thead>
														<tr>
															<th colspan="2" valign="middle" style="background-color:#e7e7e7;text-align:center;border:none;mso-line-height-rule:exactly;line-height:20.0pt;border:solid 1px #49477c;">
																<p class="MsoNormal" style="font-size:16px;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;font-weight:bold;">Courses scheduled to record
																</p>
															</th>
														</tr>
													</thead>
													<tbody>
														<#list modules as module>
															<tr style="font-size:14px;border:none;">
																<th width="25%" style="width:25%;text-align:left; padding-top:8px; valign:center; word-wrap: break-word;">
																	<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><strong>Course code </strong><span>
																</th>
																	<td width="75%" style="width:75%;text-align:left; valign:center; padding-top:8px; word-wrap: break-word;"><p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;">${module.name}</span></td>
															</tr>
															<tr>
																<th style="border:none;text-align:left; valign:center; word-wrap: break-word;">
																	<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><strong>Title </strong></span></th>
																<td>
																	<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><#if module.description?? && module.description != "">${module.description}</#if>	
																</td>
															</tr>
															<tr>
																<th>
																	<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><strong>DASS required</strong></span>
																</th>
																	<td>
																		<#if module.required>
																			<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="color: #004085; font-weight: bold; font-size:14px;">YES</span></p>
																		<#else>
																			<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;">NO</p>
																		</#if>
																	</td>
															</tr>
															<tr style="border:none;">
																<th style="border-bottom:solid #a5a5a5 1.0pt;">
																	<p class="MsoNormal" style="font-size:14px;text-align:left;padding-bottom:8px;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><strong>Camera available</strong></span>
																</th>
																<td style="padding-bottom:8px;border-bottom:solid #a5a5a5 1.0pt;">
																	<#if module.cameraAvailable>
																		<p class="MsoNormal" style="font-size:14px;text-align:left;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color: #155724; font-weight: bold; font-size:14px;">YES</span></p>
																	<#else>
																		<p class="MsoNormal" style="font-size:14px;text-align:left;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;">NO</span></p>
																	</#if>
																</td>
															</tr>      
															<tr>
																<td style="padding-bottom:8px;mso-line-height-rule:exactly; line-height:16.5pt;"></td>
															</tr>
														</#list>
													</tbody>
												</table>
												<table class="MsoTableGrid" width="642" border="0" cellspacing="0" cellpadding="0" style="width:642px;border-collapse:collapse;border:none;background:#e7e7e7;">
													<tbody>
														<tr>
															<td colspan="3" style="padding-top:5px;padding-bottom:20px;">
																<p class="MsoNormal" style="padding-left:10.0pt;padding-top:10.0pt;"><span style="font-size:12.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;text-align:left;">
																<strong>Need help?</strong></span></p>
															</td>
														</tr>
														<tr>
															<td width="33.33%" valign="top" style="width:33.33%;padding-bottom:8px;">
																<p class="MsoNormal" align="center" style="text-align:center;mso-line-height-rule:exactly;line-height:normal; padding:3.0pt; padding-top:5px;"><span style="font-family:&quot;Helvetica&quot;,sans-serif;"><a href="https://www.mypodcasts.manchester.ac.uk/staff-faqs/" style="text-decoration:none;color:#660099;font-weight:600;">FAQs</a></span></p>
															</td>
															<td width="33.33%" valign="top" style="width:33.33%;padding:none;">
																<p class="MsoNormal" align="center" style="text-align:center;mso-line-height-rule:exactly;line-height:normal;padding:3.0pt;"><span style="font-size:12.0pt;font-family:&quot;Helvetica&quot;,sans-serif;"><a href="https://www.mypodcasts.manchester.ac.uk/essential_information/" style="text-decoration:none;color:#660099;font-weight:600;">Essential Info</a></span></p>
															</td>
															<td width="33.33%" valign="top" style="width:33.33%;padding:none;">
																<p class="MsoNormal" align="center" style="text-align:center;line-height:normal; padding:3.0pt;"><span style="font-size:12.0pt;font-family:&quot;Helvetica&quot;,sans-serif;"><a href="https://twitter.com/UoMpodcast" style="text-decoration:none;color:#660099;font-weight:600;">@UoMpodcast</a></span></p>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</tbody>
												</table>
												<p class="MsoNormal" style="mso-line-height-rule:exactly;line-height:normal;text-align:center;"><span style="font-size:11.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#49477c;background:white"><a href="https://www.mypodcasts.manchester.ac.uk/privacy-notice/" style="color:#6c6c6c;">Privacy notice</a></span></p>
											</td>
										</tr>
									</tbody>
								</table>
						</tr>
					</tbody>
				</table>
			</center>
		</body>

