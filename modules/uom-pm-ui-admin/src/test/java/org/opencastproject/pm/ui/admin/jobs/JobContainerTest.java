/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.ui.admin.jobs;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.opencastproject.security.api.UnauthorizedException;
import org.opencastproject.util.data.Option;
import org.opencastproject.util.data.VCell;
import org.opencastproject.workflow.api.WorkflowDatabaseException;
import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowDefinitionImpl;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowService;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobContainerTest {
  private JobContainer jobContainer;

  @Before
  public void setUp() throws WorkflowDatabaseException, UnauthorizedException {
    WorkflowService workflowService = createMock(WorkflowService.class);

    Option<WorkflowService> option = Option.some(workflowService);
    VCell<Option<WorkflowService>> cell = new VCell<>(option, false);

    WorkflowDefinition workflowDefinition = new WorkflowDefinitionImpl();
    workflowDefinition.setId("0");

    expect(workflowService.getWorkflowInstancesByMediaPackage(null))
        .andReturn(new ArrayList<WorkflowInstance>()).anyTimes();
    replay(workflowService);

    jobContainer = new JobContainer(cell, null);

    assertEquals(0, jobContainer.size());
    assertEquals(new ArrayList<Job>(), jobContainer.getItemIds());
    assertNull(jobContainer.getItem(0));

    verify(workflowService);
  }

  @Test
  public void testRefresh() {
    assertEquals(0, jobContainer.size());

    Job job = new Job(0L, "", "", "", new Date(), "");
    List<Job> jobs = new ArrayList<>();
    jobs.add(job);

    jobContainer.addBean(job);
    jobContainer.addAll(jobs);

    assertEquals(1, jobContainer.size());

    jobContainer.refresh(null);
    assertEquals(0, jobContainer.size());
  }
}
