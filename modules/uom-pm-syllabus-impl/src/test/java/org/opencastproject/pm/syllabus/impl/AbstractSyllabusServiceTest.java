/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.syllabus.impl;

import static java.util.Collections.sort;
import static java.util.Collections.unmodifiableMap;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.opencastproject.pm.syllabus.impl.SyllabusUtil.youngest;
import static org.opencastproject.util.data.Collections.list;
import static org.opencastproject.util.data.Collections.nil;
import static org.opencastproject.util.data.Collections.toSet;
import static org.opencastproject.util.data.Monadics.mlist;
import static org.opencastproject.util.data.Option.none;
import static org.opencastproject.util.data.Option.some;

import org.opencastproject.pm.api.Action;
import org.opencastproject.pm.api.CaptureAgent;
import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.Course.EmailStatus;
import org.opencastproject.pm.api.Message;
import org.opencastproject.pm.api.Person;
import org.opencastproject.pm.api.Recording;
import org.opencastproject.pm.api.Recording.ReviewStatus;
import org.opencastproject.pm.api.Room;
import org.opencastproject.pm.syllabus.api.Occurrence;
import org.opencastproject.pm.syllabus.api.SyllabusCaptureFilter;
import org.opencastproject.pm.syllabus.api.SyllabusService;
import org.opencastproject.pm.syllabus.api.VActivity;
import org.opencastproject.pm.syllabus.api.VActivityDateTime;
import org.opencastproject.pm.syllabus.api.VActivityLocation;
import org.opencastproject.pm.syllabus.api.VActivityStaff;
import org.opencastproject.pm.syllabus.api.VActivityStudentSet;
import org.opencastproject.pm.syllabus.api.VDepartment;
import org.opencastproject.pm.syllabus.api.VLocation;
import org.opencastproject.pm.syllabus.api.VLocationSuitability;
import org.opencastproject.pm.syllabus.api.VModule;
import org.opencastproject.pm.syllabus.api.VStaff;
import org.opencastproject.pm.syllabus.api.VStudentSet;
import org.opencastproject.pm.syllabus.api.VZones;
import org.opencastproject.util.data.Either;
import org.opencastproject.util.data.Function;
import org.opencastproject.util.data.Function0;
import org.opencastproject.util.data.Option;
import org.opencastproject.util.persistence.PersistenceEnv;
import org.opencastproject.util.persistence.Queries;

import com.entwinemedia.fn.Fn;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
* THIS IS NOT A REAL UNIT TEST BUT AN INTEGRATION TEST RUNNER AND USAGE DEMO.
* ATTENTION: THIS CLASS IS OUTDATED SINCE
* THE LAST REFACTORINGS todo turn into a real unit test by creating a database
* with some test data.
*/
@RunWith(PowerMockRunner.class)
@PrepareForTest(Queries.class)
public class AbstractSyllabusServiceTest {
  private PersistenceEnv persistenceEnv;
  private EntityManager entityManager;
  private Query query;
  private Function<EntityManager,Object> tx;
  private Queries queries;
  private DateTime now = DateTime.now();

  private AbstractSyllabusService syllabusService;

  @Before
  public void setUp() {
    tx = createNiceMock(Function.class);
    query = createNiceMock(Query.class);
    entityManager = createNiceMock(EntityManager.class);
    persistenceEnv = createNiceMock(PersistenceEnv.class);
    queries = createNiceMock(Queries.class);

    syllabusService = new AbstractSyllabusService() {
      protected PersistenceEnv getPenv() {
        return persistenceEnv;
      }

      public String getSourceDescription() {
        return "Syllabus+:Test";
      }

      @Override
      protected void closePenv() {
      }
    };

    expect(persistenceEnv.tx(new Function<EntityManager, EntityManager>() {
      @Override
      public EntityManager apply(EntityManager entityManager) {
        return (EntityManager) entityManager;
      }
    })).andReturn(anyObject()).anyTimes();
  }

  @Test
  public void testFindOccurrence() throws Exception {
    List<Occurrence> mockOccurrences = new ArrayList<>();

    mockOccurrences.add(new OccurrenceImpl("1",
                                           "name",
                                           new VLocationImpl("1",
                                                             "1",
                                                             "1",
                                                             new DateTime(),
                                                             Option.some("")),
                                           new Interval(now, now),
                                           now));

    expect(persistenceEnv.tx(new Function<EntityManager, Object>() {
      @Override
      public Object apply(EntityManager entityManager) {
        return entityManager;
      }
    })).andReturn(mockOccurrences).anyTimes();

    replay(tx, query, entityManager, persistenceEnv, queries);

    final List<Occurrence> occurrences = syllabusService.findOccurrences(now, now);
    assertEquals(1, occurrences.size());

    for (Occurrence occurrence : occurrences) {
      assertEquals("1", occurrence.getActivityId());
      assertEquals("name", occurrence.getActivityName());
      assertEquals(now, occurrence.getLastChanged());
    }

    verify(tx, query, entityManager, persistenceEnv, queries);
    reset(tx, query, entityManager, persistenceEnv, queries);
  }

  // @Test
  public void testFindActivity() {
    List<VActivity> mockActivities = new ArrayList<>();

    mockActivities.add(new VActivity() {
      @Override
      public String getId() {
        return "1";
      }

      @Override
      public String getHostKey() {
        return "1";
      }

      @Override
      public DateTime getLastChanged() {
        return new DateTime(2023,
                            1,
                            1,
                            0,
                            0,
                            0);
      }

      @Override
      public String getName() {
        return "1";
      }

      @Override
      public String getActivityTypeId() {
        return "1";
      }

      @Override
      public String getDepartmentId() {
        return "1";
      }

      @Override
      public String getModuleId() {
        return "1";
      }

      @Override
      public Option<String> getDescription() {
        return Option.some("1");
      }

      @Override
      public Option<DateMidnight> getStartDate() {
        return Option.some(new DateMidnight(2023,
                                            1,
                                            1,
                                            DateTimeZone.UTC));
      }

      @Override
      public Option<DateMidnight> getEndDate() {
        return Option.some(new DateMidnight(2023,
                                            1,
                                            1,
                                            DateTimeZone.UTC));
      }

      @Override
      public boolean isScheduled() {
        return true;
      }
    });

    expect(persistenceEnv.tx(new Function<EntityManager, Object>() {
      @Override
      public Object apply(EntityManager entityManager) {
        return entityManager;
      }
    })).andReturn(mockActivities).anyTimes();

    replay(tx, query, entityManager, persistenceEnv, queries);

    final List<VActivity> activities = syllabusService.findActivities(new DateTime(2013, 7, 1, 0, 0, 0));
    assertEquals(1, activities.size());

    for (VActivity activity : activities) {
      assertEquals(new DateTime(2023,
                                1,
                                1,
                                0,
                                0,
                                0), activity.getStartDate());
    }

    verify(tx, query, entityManager, persistenceEnv, queries);
    reset(tx, query, entityManager, persistenceEnv, queries);
  }

  // @Test
  public void testFindLocation() {
    final List<VLocation> as = syllabusService.findLocations(new DateTime(2013, 7, 1, 0, 0, 0));
    for (VLocation a : as) {
      // System.out.pri ntln(a.getId());
    }
  }

  // @Test
  public void testFindActivityDateTime() {
    final List<VActivityDateTime> as = syllabusService.findActivityDateTime();
    // System.out.pri ntln(as.size());
  }

  // @Test
  public void testFindActivityLocation() {
    final List<VActivityLocation> as = syllabusService.findActivityLocation();
    // System.out.pri ntln(as.size());
  }

  private final Function<VActivityDateTime, String> getActivityIdDateTime = new Function<VActivityDateTime, String>() {
    @Override
    public String apply(VActivityDateTime a) {
      return a.getActivityId();
    }
  };

  private final Function<VStaff, String> getStaffId = new Function<VStaff, String>() {
    @Override
    public String apply(VStaff a) {
      return a.getId();
    }
  };

  private final Function<VDepartment, String> getDepartmentId = new Function<VDepartment, String>() {
    @Override
    public String apply(VDepartment a) {
      return a.getId();
    }
  };

  private final Function<VZones, String> getZonesId = new Function<VZones, String>() {
    @Override
    public String apply(VZones a) {
      return a.getId();
    }
  };

  private final Function<VStudentSet, String> getStudentId = new Function<VStudentSet, String>() {
    @Override
    public String apply(VStudentSet a) {
      return a.getId();
    }
  };

  private final Function<VModule, String> getModuleId = new Function<VModule, String>() {
    @Override
    public String apply(VModule a) {
      return a.getId();
    }
  };

  private final Function<VLocation, String> getLocationId = new Function<VLocation, String>() {
    @Override
    public String apply(VLocation a) {
      return a.getId();
    }
  };

  private final Function<VActivityStudentSet, String> getActivityIdStudentSet
      = new Function<VActivityStudentSet, String>() {
        @Override
        public String apply(VActivityStudentSet a) {
          return a.getActivityId();
        }
      };

  private final Function<VActivityStaff, String> getActivityIdStaff = new Function<VActivityStaff, String>() {
    @Override
    public String apply(VActivityStaff a) {
      return a.getActivityId();
    }
  };

  private final Function<VActivityLocation, String> getActivityIdLocation = new Function<VActivityLocation, String>() {
    @Override
    public String apply(VActivityLocation a) {
      return a.getActivityId();
    }
  };

  // Works but needs some heap memory. Test did not pass with a heap size below
  // -Xmx225m
  // @Test
  public void testBuildActivityHierarchyBruteForce() {
    final SyllabusService syl = syllabusService;
    //
    // Slurp everything into memory. This brute force approach requires some heap
    // space.
    final List<VActivity> activities = syl.findActivities();
    // System.out.pri ntln("# activity " + activities.size());
    final List<VLocation> locationList = syl.findLocations();
    // System.out.pri ntln("# location " + locationList.size());
    final Map<String, VLocation> locationMap = asMap(locationList, getLocationId);
    final List<VActivityLocation> activityLocationList = syl.findActivityLocation();
    // System.out.pri ntln("# activityLocation " + activityLocationList.size());
    final Multimap<String, VActivityLocation> activityLocationMap = groupBy(
        ArrayListMultimap.<String, VActivityLocation>create(), activityLocationList, getActivityIdLocation);
    final List<VActivityDateTime> activityDateTimeList = syl.findActivityDateTime();
    // System.out.pri ntln("# activityDateTime " + activityDateTimeList.size());
    final Multimap<String, VActivityDateTime> activityDateTimeMap = groupBy(
        ArrayListMultimap.<String, VActivityDateTime>create(), activityDateTimeList, getActivityIdDateTime);
    //
    // create graph
    int i = 0;
    for (VActivity activity : activities) {
      final String aId = activity.getId();
      // just looking for a particular activity id
      // if (ne("EAF0151470EB2655C6303E13C954BE19", aId))
      // continue;
      for (VActivityDateTime dateTime : activityDateTimeMap.get(aId)) {
        for (VActivityLocation activityLocation : activityLocationMap.get(aId)) {
          final VLocation location = locationMap.get(activityLocation.getLocationId());
          final Date lastModification = youngest(activity.getLastChanged(), dateTime.getLastChanged(),
              activityLocation.getLastChanged(), location.getLastChanged()).toDate();
          final Recording rec = Recording.recording(aId, activity.getName(), false, nil(Person.class), // todo
              null, // todo
              new Room(location.getName()), lastModification,
              // think about adding a configurable offset to start
              // and stop dates since they describe when the event
              // actually happens. CA should probably capture a bigger
              // time frame in order to not miss anything.
              // This offset may be applied when creating the MH schedule.
              // - or, the recording may hold the offsets
              // - or, the recording may hold a capture start date, event start date
              // and a capture end date and an event end date
              dateTime.getStartDateTime().toDate(), dateTime.getEndDateTime().toDate(), nil(Person.class), // todo
              nil(Message.class), // todo
              some("0"), // todo
              null, // todo
              nil(Action.class), EmailStatus.UNSENT, ReviewStatus.UNCONFIRMED, null, false, false, "screen");
          // todo store in database
          // System.out.pri ntln(String.format("%6d ", ++i) + "Recording " +
          // rec.getActivityId() + " "
          // + new DateTime(rec.getStart()) + " " + new DateTime(rec.getStop()) + " @ " +
          // rec.getRoom().getName());
        }
      }
    }

    // for (VActivity activity : activities) {
    // Collection<VActivityLocation> locations =
    // activityLocationMap.get(activity.getId());
    // if (locations.size() > 1) {
    // mlist(locations.iterator()).foldl(Collections.<String>set(), new
    // Function2<Set<String>, VActivityLocation,
    // Set<String>>() {
    // @Override public Set<String> apply(Set<String> sum, VActivityLocation a) {
    // sum.add(a.getLocationId());
    // return sum;
    // }
    // });
    // System.out.pri ntln("Activity " + activity.getId() + " happens in " +
    // locations.size() + " locations");
    // for (VActivityLocation location : locations) {
    // System.out.pri ntln(" " + location.getActivityId() + " -> " +
    // location.getLocationId());
    // }
    // }
    // }

    // System.out.pri ntln("Activity B5BA6B3E7FA1CD9114286BA4ACEB9933");
    // for (VActivityLocation location :
    // activityLocationMap.get("B5BA6B3E7FA1CD9114286BA4ACEB9933")) {
    // System.out.pri ntln(location.getLocationId());
    // }
  }

  // Seems to issue too many requests so that it hangs reproducible.
  // @Test
  public void testBuildActivityHierarchyLessMemory() {
    final SyllabusService syl = syllabusService;
    //
    // Slurp everything into memory except the V_ACTIVITY_DATETIME table. Uses less
    // memory than
    // the previous brute force approach but issues one additional request per
    // activity which
    // is currently 22273! Hangs after creating the 1031th recording.
    final List<VActivity> activities = syl.findActivities();
    // System.out.pri ntln("# activity " + activities.size());
    final List<VLocation> locationList = syl.findLocations();
    // System.out.pri ntln("# location " + locationList.size());
    final Map<String, VLocation> locationMap = asMap(locationList, getLocationId);
    final List<VActivityLocation> activityLocationList = syl.findActivityLocation();
    // System.out.pri ntln("# activityLocation " + activityLocationList.size());
    final Multimap<String, VActivityLocation> activityLocationMap = groupBy(
        ArrayListMultimap.<String, VActivityLocation>create(), activityLocationList, getActivityIdLocation);
    //
    // create graph
    int i = 0;
    for (VActivity activity : activities) {
      final String aId = activity.getId();
      // if (ne("EAF0151470EB2655C6303E13C954BE19", aId))
      // continue;
      // issue a new database request for each activity
      for (VActivityDateTime dateTime : syl.findActivityDateTime(aId)) {
        for (VActivityLocation activityLocation : activityLocationMap.get(aId)) {
          final VLocation location = locationMap.get(activityLocation.getLocationId());
          final Date lastModification = youngest(activity.getLastChanged(), dateTime.getLastChanged(),
              activityLocation.getLastChanged(), location.getLastChanged()).toDate();
          final Recording rec = Recording.recording(aId, activity.getName(), false, nil(Person.class), // todo
              null, // todo
              new Room(location.getName()), lastModification,
              // think about adding a configurable offset to start
              // and stop dates since they describe when the event
              // actually happens. CA should probably capture a bigger
              // time frame in order to not miss anything.
              // This offset may be applied when creating the MH schedule.
              // - or, the recording may hold the offsets
              // - or, the recording may hold a capture start date, event start date
              // and a capture end date and an event end date
              dateTime.getStartDateTime().toDate(), dateTime.getEndDateTime().toDate(), nil(Person.class), // todo
              nil(Message.class), // todo
              some("0"), // todo
              null, // todo
              nil(Action.class), EmailStatus.UNSENT, ReviewStatus.UNCONFIRMED, null, false, false, "screen");
          // todo store in database
          // System.out.pri ntln(String.format("%6d ", ++i) + "Recording " +
          // rec.getActivityId() + " "
          // + new DateTime(rec.getStart()) + " " + new DateTime(rec.getStop()) + " @ " +
          // rec.getRoom().getName());
        }
      }
    }
  }

  // Seems to issue too many requests so that it hangs reproducible.
  // @Test
  public void testBuildActivityHierarchyEvenLessMemory() {
    final SyllabusService syl = syllabusService;
    //
    // Slurp everything into memory except the V_ACTIVITY_DATETIME and
    // V_ACTIVITY_LOCATION table.
    // Issues probably far too many requests. Hangs like the previous approach after
    // the creation of the 1031th recording.
    final List<VActivity> activities = syl.findActivities();
    // System.out.pri ntln("# activity " + activities.size());
    final List<VLocation> locationList = syl.findLocations();
    // System.out.pri ntln("# location " + locationList.size());
    final Map<String, VLocation> locationMap = asMap(locationList, getLocationId);
    //
    // create graph
    int i = 0;
    for (VActivity activity : activities) {
      final String aId = activity.getId();
      // limit to a certain activity id
      // if (ne("EAF0151470EB2655C6303E13C954BE19", aId))
      // continue;
      // issue a new database request for each activity to keep memory consumption low
      final List<VActivityDateTime> activityDateTimeList = syl.findActivityDateTime(aId);
      final List<VActivityLocation> activityLocationList = syl.findActivityLocation(aId);
      for (VActivityDateTime dateTime : activityDateTimeList) {
        // issue a new database request for each activity
        for (VActivityLocation activityLocation : activityLocationList) {
          final VLocation location = locationMap.get(activityLocation.getLocationId());
          final Date lastModification = youngest(activity.getLastChanged(), dateTime.getLastChanged(),
              activityLocation.getLastChanged(), location.getLastChanged()).toDate();
          final Recording rec = Recording.recording(aId, activity.getName(), false, nil(Person.class), // todo
              null, // todo
              new Room(location.getName()), lastModification,
              // think about adding a configurable offset to start
              // and stop dates since they describe when the event
              // actually happens. CA should probably capture a bigger
              // time frame in order to not miss anything.
              // This offset may be applied when creating the MH schedule.
              // - or, the recording may hold the offsets
              // - or, the recording may hold a capture start date, event start date
              // and a capture end date and an event end date
              dateTime.getStartDateTime().toDate(), dateTime.getEndDateTime().toDate(), nil(Person.class), // todo
              nil(Message.class), // todo
              some("0"), // todo
              null, // todo
              nil(Action.class), EmailStatus.UNSENT, ReviewStatus.UNCONFIRMED, null, false, false, "screen");
          // todo store in database
          // System.out.pri ntln(String.format("%6d ", ++i) + "Recording " +
          // rec.getActivityId() + " "
          // + new DateTime(rec.getStart()) + " " + new DateTime(rec.getStop()) + " @ " +
          // rec.getRoom().getName());
        }
      }
    }
  }

  // This test partitions the list of activities into chunks of 1000 and then
  // fetches the matching
  // V_ACTIVITY_DATETIME entities. This test runs with a heap size of -Xmx75m and
  // has a reasonable
  // performance.
  // @Test
  public void testBuildActivityHierarchyPartitioned() {
    final SyllabusService syl = syllabusService;
    final SyllabusCaptureFilter captureRooms = new SyllabusCaptureFilterImpl();
    //
    // Slurp some tables into memory. Then partition the activity list and fetch
    // V_ACTIVITY_DATETIME
    // in partitions.
    final List<List<VActivity>> activitiesPartitioned;
    {
      // Put this in a separate method or like this so that the garbage collector can
      // dispose the original list to safe heap space.
      // Drain list into new list since the returned one is immutable
      final List<VActivity> activities = new ArrayList<VActivity>(syl.findActivities());
      // Sort activities by id ascending. Sorting is crucial otherwise the
      // partition approach does not work. Matching dateTime entities are fetched
      // based an lexicographic id ordering
      sort(activities, new Comparator<VActivity>() {
        @Override
        public int compare(VActivity a, VActivity b) {
          return a.getId().compareTo(b.getId());
        }
      });
      // System.out.pri ntln("# activity " + activities.size());
      activitiesPartitioned = grouped(activities, 1000);
    }
    final List<VLocation> locationList = syl.findLocations();
    // System.out.pri ntln("# location " + locationList.size());
    final Map<String, VLocation> locationMap = asMap(locationList, getLocationId);
    final List<VActivityLocation> activityLocationList = syl.findActivityLocation();
    // System.out.pri ntln("# activityLocation " + activityLocationList.size());
    final Multimap<String, VActivityLocation> activityLocationMap = groupBy(
        ArrayListMultimap.<String, VActivityLocation>create(), activityLocationList, getActivityIdLocation);

    List<VModule> modules = syl.findModules();
    // System.out.pri ntln("# modules " + modules.size());
    final Map<String, VModule> moduleMap = asMap(modules, getModuleId);

    List<VDepartment> department = syl.findDepartment();
    // System.out.pri ntln("# departments " + department.size());
    final Map<String, VDepartment> departmentMap = asMap(department, getDepartmentId);

    List<VZones> zones = syl.findZones();
    // System.out.pri ntln("# zones " + zones.size());
    final Map<String, VZones> zonesMap = asMap(zones, getZonesId);

    List<VStaff> vStaff = syl.findStaff();
    // System.out.pri ntln("# staff " + vStaff.size());
    final Map<String, VStaff> staffMap = asMap(vStaff, getStaffId);
    final List<VActivityStaff> activityStaffList = syl.findActivityStaff();
    // System.out.pri ntln("# activityStaff " + activityStaffList.size());
    final Multimap<String, VActivityStaff> activityStaffMap = groupBy(
        ArrayListMultimap.<String, VActivityStaff>create(), activityStaffList, getActivityIdStaff);

    List<VStudentSet> studentSet = syl.findStudentSets();
    // System.out.pri ntln("# studentSet " + studentSet.size());
    final Map<String, VStudentSet> studentMap = asMap(studentSet, getStudentId);
    final List<VActivityStudentSet> activityStudentSet = syl.findActivityStudentSet();
    // System.out.pri ntln("# activityStudentSet " + activityStudentSet.size());
    final Multimap<String, VActivityStudentSet> activityStudentSetMap = groupBy(
        ArrayListMultimap.<String, VActivityStudentSet>create(), activityStudentSet, getActivityIdStudentSet);
    final Set<String> locationHasCaptureAgent;
    {
      final List<VLocationSuitability> al = syl.findLocationSuitability();
      // System.out.pri ntln("# locationSuitability " + al.size());
      final Set<String> as = toSet(mlist(al).bind(new Function<VLocationSuitability, Option<String>>() {
        @Override
        public Option<String> apply(VLocationSuitability a) {
          return captureRooms.getCaptureRoomTypeIDs().contains(a.getSuitabilityId()) ? some(a.getLocationId())
              : none(String.class);
        }
      }).value());
      // System.out.pri ntln("# locations featuring capture agents " + as.size());
      locationHasCaptureAgent = as;
    }

    //
    // create graph
    int i = 0;
    // iterate activity partitions
    for (List<VActivity> activityPartition : activitiesPartitioned) {
      // fetch matching V_ACTIVITY_DATETIME entities
      final String firstActivityId = activityPartition.get(0).getId();
      final String lastActivityId = activityPartition.get(activityPartition.size() - 1).getId();
      final List<VActivityDateTime> activityDateTimeList = syl.findActivityDateTimeByRange(firstActivityId,
          lastActivityId);
      // System.out.pri ntln("# activityDateTime from " + firstActivityId + " to " +
      // lastActivityId + " "
      // + activityDateTimeList.size());
      final Multimap<String, VActivityDateTime> activityDateTimeMap = groupBy(
          ArrayListMultimap.<String, VActivityDateTime>create(), activityDateTimeList, getActivityIdDateTime);
      // iterate activity partition
      for (VActivity activity : activityPartition) {
        final String aId = activity.getId();
        if (!captureRooms.isCaptureActivityType(activity)) {
          continue;
        }
        for (VActivityDateTime dateTime : activityDateTimeMap.get(aId)) {
          for (VActivityLocation activityLocation : activityLocationMap.get(aId)) {
            final VLocation location = locationMap.get(activityLocation.getLocationId());
            if (location == null) {
              // System.err.pri ntln("Location " + activityLocation.getLocationId() + " not
              // found! Inconsistend data!");
              continue;
            }
            // CHECKSTYLE:OFF
            if (locationHasCaptureAgent.contains(location.getId())) {
              // CHECKSTYLE:ON
              VModule module = moduleMap.get(activity.getModuleId());

              if (location.getZoneId().isSome()) {
                VZones zone = zonesMap.get(location.getZoneId().get());
              }

              Collection<VActivityStaff> staffList = activityStaffMap.get(aId);
              if (staffList == null) {
                // System.err.pri ntln("ActivityStaffList for activity id" + aId + " not found!
                // Inconsistend data!");
                continue;
              }

              List<Person> staff = new ArrayList<Person>();
              for (VActivityStaff activityStaff : staffList) {
                VStaff s = staffMap.get(activityStaff.getStaffId());
                if (s == null) {
                  // System.err.pri ntln("Staff " + activityStaff.getStaffId() + " not found!
                  // Inconsistend data!");
                  continue;
                }
                if (s.getEmail().isSome()) {
                  staff.add(Person.person(s.getName(), s.getEmail().get()));
                }
              }

              Collection<VActivityStudentSet> participationList = activityStudentSetMap.get(aId);
              if (participationList == null) {
                // System.err.pri ntln("ActivityStudentSetList for activity id" + aId + " not
                // found! Inconsistend data!");
                continue;
              }
              List<Person> participation = new ArrayList<Person>();
              for (VActivityStudentSet activityStudent : participationList) {
                VStudentSet s = studentMap.get(activityStudent.getStudentSetId());
                if (s == null) {
                  // System.err.pri ntln("Student " + activityStudent.getStudentSetId() + " not
                  // found! Inconsistend data!");
                  continue;
                }

                if (s.getHostKey() != null && !s.getHostKey().startsWith("#") && s.getEmail().isSome()) {
                  participation.add(Person.person(s.getName(), s.getEmail().get()));
                }
              }

              if (staff.size() < 1 || module == null || StringUtils.isBlank(module.getName())) {
                continue;
              }

              Course course = new Course(module.getId(), null, null);

              final Room room = new Room(location.getName());

              final Recording rec = Recording.recording(
                  aId,
                  module.getName(),
                  staff,
                  course,
                  room,
                  new Date(),
                  // think about adding a configurable offset to start
                  // and stop dates since they describe when the event
                  // actually happens. CA should probably capture a bigger
                  // time frame in order to not miss anything.
                  // This offset may be applied when creating the MH schedule.
                  // - or, the recording may hold the offsets
                  // - or, the recording may hold a capture start date, event start date
                  // and a capture end date and an event end date
                  dateTime.getStartDateTime().toDate(), dateTime.getEndDateTime().toDate(), participation,
                  new CaptureAgent(room, CaptureAgent.getMhAgentIdFromRoom(room), "screen"));
              // todo store in database
              // System.out.pri ntln(String.format("%6d ", ++i) + "Recording " +
              // rec.getActivityId() + " "
              // + new DateTime(rec.getStart()) + " " + new DateTime(rec.getStop()) + " @ "
              // + rec.getRoom().getName());
            }
          }
        }
      }
    }
  }

   // @Test
  public void testActivityLocation() {
    for (VActivityLocation al : syllabusService.findActivityLocation()) {
      if ("B5BA6B3E7FA1CD9114286BA4ACEB9933".equals(al.getActivityId())) {
        // System.out.pri ntln(al.getLocationId());
      }
    }
  }

  // @Test
  public void testSortDateTime() {
    final List<DateTime> dates = list(new DateTime(2015, 1, 1, 0, 0, 0, 0), new DateTime(2013, 11, 15, 0, 0, 0, 0));
    // sort ascending
    sort(dates);
    assertEquals(new DateTime(2013, 11, 15, 0, 0, 0, 0), dates.get(0));
  }

  /**
   * Partition a list after some predicate <code>keyGen</code>. The partition
   * function has to make sure that keys are
   * unique per list element because each key holds only one value. Later values
   * overwrite newer ones.
   *
   * The resulting map is an immutable {@link java.util.HashMap}.
   *
   * @see #asMap(java.util.Map, java.util.List, Function)
   */
  public static <K, V> Map<K, V> asMap(List<V> values, Function<V, K> keyGen) {
    return unmodifiableMap(asMap(new HashMap<K, V>(), values, keyGen));
  }

  /**
   * Partition a list after some predicate <code>keyGen</code> into
   * <code>map</code>. The partition function has to make
   * sure that keys are unique per list element because each key holds only one
   * value. Later values overwrite newer
   * ones.
   *
   * @see #asMap(java.util.List, Function)
   */
  public static <K, V> Map<K, V> asMap(Map<K, V> map, List<V> values, Function<V, K> keyGen) {
    for (V value : values) {
      final K key = keyGen.apply(value);
      map.put(key, value);
    }
    return map;
  }

  /** Partition a list in chunks of size <code>size</code>. The last chunk may be smaller. */
  public static <A> List<List<A>> grouped(List<A> as, int size) {
    final List<List<A>> grouped = new ArrayList<>((as.size() / size) + 1);
    List<A> group = new ArrayList<>(size);
    grouped.add(group);
    int count = size;
    for (A a : as) {
      if (count == 0) {
        group = new ArrayList<>(size);
        grouped.add(group);
        count = size;
      }
      group.add(a);
      count--;
    }
    return grouped;
  }

  /**
   * Partition a list after some predicate <code>group</code> into <code>map</code>.
   *
   * Use e.g. <code>ArrayListMultimap.create()</code> to create a multimap.
   *
   * @see #groupBy(Iterable, Function)
   */
  public static <K, V> Multimap<K, V> groupBy(Multimap<K, V> map,
                                              Iterable<? extends V> values,
                                              Function<? super V, ? extends K> group) {
    for (V value : values) {
      final K key = group.apply(value);
      map.put(key, value);
    }
    return map;
  }

  /**
   * Partition a list after some predicate <code>group</code> into <code>map</code>.
   *
   * @return an {@link ImmutableMultimap}
   * @see #groupBy(com.google.common.collect.Multimap, Iterable, Function)
   */
  public static <K, V> ImmutableMultimap<K, V> groupBy(Iterable<? extends V> values,
                                                       Function<? super V, ? extends K> group) {
    final ImmutableMultimap.Builder<K, V> map = ImmutableMultimap.builder();
    for (V value : values) {
      final K key = group.apply(value);
      map.put(key, value);
    }
    return map.build();
  }

  /** Load a properties file from the classpath using the class loader of {@link org.opencastproject.util.IoSupport}. */
  public static Properties loadPropertiesFromClassPath(String resource) {
    return loadPropertiesFromClassPath(resource, AbstractSyllabusService.class);
  }

  /** Load a properties file from the classpath using the class loader of the given class. */
  public static Properties loadPropertiesFromClassPath(final String resource, final Class<?> clazz) {
    for (InputStream in : openClassPathResource(resource, clazz)) {
      return withResource(in, new Function<InputStream, Properties>() {
        @Override
        public Properties apply(InputStream is) {
          final Properties p = new Properties();
          try {
            p.load(is);
          } catch (Exception e) {
            throw new Error("Cannot load resource " + resource + "@" + clazz);
          }
          return p;
        }
      });
    }
    return chuck(new FileNotFoundException(resource + " does not exist"));
  }

  /**
   * Handle a closeable resource inside <code>f</code> and ensure it gets closed properly.
   */
  public static <A, B extends Closeable> A withResource(B b, Function<B, A> f) {
    try {
      return f.apply(b);
    } finally {
      closeQuietly(b);
    }
  }

  /**
   * Handle a closeable resource inside <code>f</code> and ensure it gets closed properly.
   */
  public static <A, B extends Closeable> A withResource(B b, Fn<? super B, ? extends A> f) {
    try {
      return f.apply(b);
    } finally {
      closeQuietly(b);
    }
  }

  /**
   * Open a classpath resource using the class loader of the given class.
   *
   * @return an input stream to the resource wrapped in a Some or none if the resource cannot be found
   */
  public static Option<InputStream> openClassPathResource(String resource, Class<?> clazz) {
    return Option.option(clazz.getResourceAsStream(resource));
  }

  /**
   * Open a classpath resource using the class loader of {@link org.opencastproject.util.IoSupport}.
   *
   * @see #openClassPathResource(String, Class)
   */
  public static Option<InputStream> openClassPathResource(String resource) {
    return openClassPathResource(resource, AbstractSyllabusService.class);
  }

  /**
   * Handle a closeable resource inside <code>f</code> and ensure that <code>r</code> gets closed properly.
   *
   * @param r
   *          resource creation function
   * @param toErr
   *          error handler transforming an exception into something else
   * @param f
   *          resource handler
   */
  public static <A, Err, B extends Closeable> Either<Err, A> withResource(Function0<B> r,
          Function<Exception, Err> toErr, Function<B, A> f) {
    B b = null;
    try {
      b = r.apply();
      return null;
      // return right(f.apply(b));
    } catch (Exception e) {
      return null;
      // return left(toErr.apply(e));
    } finally {
      closeQuietly(b);
    }
  }

  public static boolean closeQuietly(final Closeable s) {
    if (s == null) {
      return false;
    }
    try {
      s.close();
      return true;
    } catch (IOException e) {
      return false;
    }
  }

  /**
   * Throw a checked exception like a RuntimeException removing any needs to declare a throws clause.
   *
   * This technique has been described by James Iry at
   * http://james-iry.blogspot.de/2010/08/on-removing-java-checked-exceptions-by.html
   */
  public static <A> A chuck(Throwable t) {
    try {
      throw t;
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return null;
  }

  /** {@link #chuck(Throwable)} as a function. */
  public static <A extends Throwable, B> Function<A, B> chuck() {
    return new Function<>() {
      @Override
      public B apply(Throwable throwable) {
        return chuck(throwable);
      }
    };
  }
}
