/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.syllabus.impl.scheduling;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabase;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabaseException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import uk.ac.manchester.requirement.RequirementService;
import uk.ac.manchester.requirement.RequirementServiceException;

@RunWith(value = BlockJUnit4ClassRunner.class)
public class DassRequirementManagerTest {
  private RequirementService requirementService;
  private ParticipationManagementDatabase database;

  private DassRequirementManager classToTest;

  @Before
  public void setUp() {
    requirementService = createNiceMock(RequirementService.class);
    database = createNiceMock(ParticipationManagementDatabase.class);
  }

  @After
  public void tearDown() {
    verify(requirementService, database);
  }

  @Test
  public void testWithoutCourses() throws RequirementServiceException, ParticipationManagementDatabaseException {
    AtomicInteger functionsCalled = new AtomicInteger();

    classToTest = new DassRequirementManager(requirementService, database);
    expect(requirementService.getIds(
            RequirementService.Resource.SERIES, RequirementService.Requirement.RECORDING)).andAnswer(() -> {
              functionsCalled.incrementAndGet();
              return new ArrayList<>();
            });
    expect(database.getCourses()).andAnswer(() -> {
      functionsCalled.incrementAndGet();
      return new ArrayList<>();
    });

    replay(requirementService, database);

    classToTest.updateRequirements();

    assertEquals(2, functionsCalled.get());
  }

  @Test
  public void testWithRequiredCourseWithExternalCourseKeyRecord()
          throws RequirementServiceException, ParticipationManagementDatabaseException {
    AtomicInteger functionsCalled = new AtomicInteger();

    List<Course> courses = new ArrayList<>();
    Course course = new Course("0");
    courses.add(course);

    List<String> requirements = new ArrayList<>();
    requirements.add(Course.REQUIREMENT_RECORD);

    List<String> requiredCourses = new ArrayList<>();
    course.setExternalCourseKey(Course.REQUIREMENT_RECORD);
    course.setRequirements(requirements);
    requiredCourses.add("0");

    classToTest = new DassRequirementManager(requirementService, database);
    expect(requirementService.getIds(
            RequirementService.Resource.SERIES, RequirementService.Requirement.RECORDING)).andAnswer(() -> {
              functionsCalled.incrementAndGet();
              return requiredCourses;
            });
    expect(database.getCourses()).andAnswer(() -> {
      functionsCalled.incrementAndGet();
      return courses;
    });

    replay(requirementService, database);

    classToTest.updateRequirements();

    assertEquals(2, functionsCalled.get());
  }

  @Test
  public void testWithRequiredCourseWithExternalCourseKey0()
          throws RequirementServiceException, ParticipationManagementDatabaseException {
    AtomicInteger functionsCalled = new AtomicInteger();

    List<Course> courses = new ArrayList<>();
    Course course = new Course("0");
    courses.add(course);

    List<String> requirements = new ArrayList<>();
    requirements.add("0");

    List<String> requiredCourses = new ArrayList<>();
    course.setExternalCourseKey("0");
    course.setRequirements(requirements);
    requiredCourses.add("0");

    classToTest = new DassRequirementManager(requirementService, database);
    expect(requirementService.getIds(
            RequirementService.Resource.SERIES, RequirementService.Requirement.RECORDING)).andAnswer(() -> {
              functionsCalled.incrementAndGet();
              return requiredCourses;
            });
    expect(database.getCourses()).andAnswer(() -> {
      functionsCalled.incrementAndGet();
      return courses;
    });

    replay(requirementService, database);

    classToTest.updateRequirements();

    assertEquals(2, functionsCalled.get());
  }

  @Test
  public void testWithoutRequiredCourse() throws RequirementServiceException, ParticipationManagementDatabaseException {
    AtomicInteger functionsCalled = new AtomicInteger();

    List<Course> courses = new ArrayList<>();
    Course course = new Course("0");
    courses.add(course);

    List<String> requiredCourses = new ArrayList<>();
    requiredCourses.add("1");

    classToTest = new DassRequirementManager(requirementService, database);
    expect(requirementService.getIds(
            RequirementService.Resource.SERIES, RequirementService.Requirement.RECORDING)).andAnswer(() -> {
              functionsCalled.incrementAndGet();
              return requiredCourses;
            });
    expect(database.getCourses()).andAnswer(() -> {
      functionsCalled.incrementAndGet();
      return courses;
    });

    replay(requirementService, database);

    classToTest.updateRequirements();

    assertEquals(2, functionsCalled.get());
  }
}
