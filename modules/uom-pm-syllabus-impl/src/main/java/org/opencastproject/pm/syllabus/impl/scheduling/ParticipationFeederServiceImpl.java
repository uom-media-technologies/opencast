/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.syllabus.impl.scheduling;

import static org.opencastproject.util.OsgiUtil.getCfg;
import static org.opencastproject.util.OsgiUtil.getOptCfg;
import static org.opencastproject.util.data.Monadics.mlist;
import static org.opencastproject.util.data.Option.some;
import static org.opencastproject.util.data.Tuple.tuple;
import static org.opencastproject.util.data.VCell.ocell;
import static org.opencastproject.util.data.functions.Strings.toBool;

import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.SchedulingSource;
import org.opencastproject.pm.api.Synchronization;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabase;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabaseException;
import org.opencastproject.pm.api.scheduling.ParticipationFeederService;
import org.opencastproject.pm.api.util.RequirementManager;
import org.opencastproject.pm.syllabus.api.SyllabusData;
import org.opencastproject.pm.syllabus.api.SyllabusDataService;
import org.opencastproject.pm.syllabus.api.VActivity;
import org.opencastproject.pm.syllabus.api.VModule;
import org.opencastproject.security.api.Organization;
import org.opencastproject.security.api.OrganizationDirectoryService;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.util.SecurityContext;
import org.opencastproject.security.util.SecurityUtil;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Function;
import org.opencastproject.util.data.Option;
import org.opencastproject.util.data.Tuple;
import org.opencastproject.util.data.VCell;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.ws.rs.core.Response;

import uk.ac.manchester.requirement.RequirementService;

/** Connects the {@link ParticipationFeederRunner} to the OSGi environment. */
public class ParticipationFeederServiceImpl implements ManagedService, ParticipationFeederService {
  public static final boolean DEFAULT_RUN_ON_START = false;
  public static final boolean DEFAULT_SCHEDULE = false;

  /** Log facility */
  private static final Logger logger = LoggerFactory.getLogger(ParticipationFeederRunner.class);

  // Dependencies
  private SyllabusDataService syllabusDataService;
  private ParticipationManagementDatabase persistence;
  private SecurityService securityService;
  private OrganizationDirectoryService organizationDirectoryService;
  private RequirementService requirementService;
  private RequirementManager requirementManager;

  private String systemUser;

  private final VCell<Option<SecurityContext>> secCtx = ocell();

  private ParticipationFeederRunner runner;

  /** OSGi container callback. */
  public void setSyllabusDataService(SyllabusDataService syllabusDataService) {
    this.syllabusDataService = syllabusDataService;
  }

  /** OSGi container callback. */
  public void setPersistence(ParticipationManagementDatabase persistence) {
    this.persistence = persistence;
  }

  /** OSGi container callback. */
  public void setSecurityService(SecurityService securityService) {
    this.securityService = securityService;
  }

  /** OSGi container callback. */
  public void setOrganizationDirectoryService(OrganizationDirectoryService organizationDirectory) {
    this.organizationDirectoryService = organizationDirectory;
  }

  /** OSGi container callback. */
  public void setRequirementService(RequirementService requirementService) {
    this.requirementService = requirementService;
  }

  /** OSGi container callback. */
  public synchronized void activate(final ComponentContext cc) {
    requirementManager = new DassRequirementManager(requirementService, persistence);
    systemUser = cc.getBundleContext().getProperty(SecurityUtil.PROPERTY_KEY_SYS_USER);
    runner = new ParticipationFeederRunner(syllabusDataService, persistence, requirementManager, secCtx);
  }

  /** OSGi container callback. */
  public synchronized void deactivate(ComponentContext cc) {
    runner.shutdown();
  }

  @Override
  public Response permitHarvest() {
    try {
      final Synchronization sync = persistence.getLastSynchronization();
      if (null == sync) {
        Response.ResponseBuilder respb = Response.status(Response.Status.PRECONDITION_FAILED);
        respb.type("Can't trigger initial harvest externally, try harvesting through UI!");
        return respb.build();
      }

    } catch (ParticipationManagementDatabaseException ex) {
      java.util.logging.Logger.getLogger(ParticipationFeederServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      Response.ResponseBuilder respb = Response.status(Response.Status.SERVICE_UNAVAILABLE);
      respb.type("DB Problem");
      return respb.build();
    }
    return Response.ok().build();
  }

  @Override
  public void harvest() {
    runner.trigger();
  }

  protected void waitForSync(final Synchronization sync) {
    SecurityContext securityContext = secCtx.get().get();
    securityContext.runInContext(() -> {
      while (sync != null && (runner.isRunning())) {
        try {
          Thread.sleep(0);
        } catch (InterruptedException ignore) {
        }
      }
    });
  }

  @Override
  public Response getHarvestStats() throws NotFoundException {
    try {
      final Synchronization sync = persistence.getLastSynchronization();
      waitForSync(sync);
      HarvestStats stats = runner.getLastHarvestStats();
      long storedTotalRecordings = persistence.countTotalRecordings();
      int totalSplusRecordings = stats.getTotal();
      if ((storedTotalRecordings - totalSplusRecordings) > -100) {
        Response.ResponseBuilder respb = Response.status(Response.Status.PRECONDITION_FAILED);
        respb.type("Trying to delete too many Recordings -- please verify");
        return respb.build();
      }

    } catch (ParticipationManagementDatabaseException ex) {
      java.util.logging.Logger.getLogger(ParticipationFeederServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok().build();
  }

  @Override
  public Course getCourseByActivityId(String activityId) throws NotFoundException {
    final SyllabusData data = syllabusDataService.fetchModules();
    final ModuleFinder moduleFinder = new ModuleFinder(data.getModule(), data.getActivityParent(), data.getActivity());
    final VActivity activity = data.getActivity().get(activityId);
    final String sourceDescription = data.getSourceDescription();

    if (activity == null) {
      throw(new NotFoundException());
    }

    // Assume activity is a child node
    final List<VActivity> activityHierarchy = moduleFinder.collectUntilModule(activity);
    final List<VModule> modules = moduleFinder.getModules(activityHierarchy);
    Course course = ParticipationFeederRunner.mergeModules(modules);
    course.setSeriesId(course.createSeriesId());

    if (StringUtils.isNotBlank(sourceDescription)) {
      course.setSchedulingSource(Option.some(new SchedulingSource(sourceDescription)));
    }

    return course;
  }



   /* NOTE
   * Should only be called if there are no activities and therefore no chance of
   * being a combined course
   */
  @Override
  public Course getCourseByCourseKeys(List<String> courseKeys) throws NotFoundException {
    Course course = null;
    List<Course> courses;

    // First see if course exists
    try {
      courses = persistence.findCoursesByCourseKey(courseKeys.get(0));
    } catch (ParticipationManagementDatabaseException e) {
      logger.error("Unable to search for Course {}", course.getExternalCourseKey());
      throw(new NotFoundException(e));
    }

    if (courses.isEmpty()) {
      // Create a new course
      final List<VModule> modules = new ArrayList<VModule>();
      for (String courseKey : courseKeys) {
        final VModule module = syllabusDataService.getModuleByCourseKey(courseKey);

        if (module != null) {
          modules.add(module);
        } else {
          throw (new NotFoundException());
        }
      }
      course = ParticipationFeederRunner.mergeModules(modules);
      course.setSeriesId(course.createSeriesId());
      try {
        persistence.updateCourse(course);
      } catch (ParticipationManagementDatabaseException e) {
        logger.error("Unable to persist Course {}", course.getExternalCourseKey());
      }
    } else {
      if (courses.size() > 1) {
        logger.warn("Multiple Course entries found for {}", courseKeys.get(0));
      }
      course = courses.get(0);
    }

    return course;
  }

  @Override
  public List<String> getActivityIds(String courseKey) throws NotFoundException {
    return syllabusDataService.findModuleActivityIdsByCourseKey(courseKey);
  }

  /** OSGi container called (ConfigurationAdmin). */
  @Override
  public synchronized void updated(Dictionary properties) throws ConfigurationException {
    if (properties != null) {
      // read config
      final String cron = getCfg(properties, "cron");
      final String orgId = getCfg(properties, "organization");
      final boolean runOnStart = getOptCfg(properties, "run-on-start").map(toBool).getOrElse(DEFAULT_RUN_ON_START);
      final boolean schedule = getOptCfg(properties, "schedule").map(toBool).getOrElse(DEFAULT_SCHEDULE);

      // create security context
      final Organization org;
      try {
        org = organizationDirectoryService.getOrganization(orgId);
      } catch (NotFoundException e) {
        throw new ConfigurationException("organization", "not found", e);
      }
      secCtx.set(some(new SecurityContext(securityService, org, SecurityUtil.createSystemUser(systemUser, org))));
      //
      if (schedule) {
        runner.schedule(cron);
      }

      if (runOnStart) {
        runner.trigger();
      }
      //
      logger.info(showConfig(tuple("cron", cron), tuple("organization", org), tuple("run-on-start", runOnStart),
              tuple("schedule", schedule)));
    }
  }

  @Deprecated
  public static String showConfig(Tuple<String, ?>... cfg) {
    return "Config\n" + mkString(mlist(cfg).map(new Function<Tuple<String, ?>, String>() {
      @Override
      public String apply(Tuple<String, ?> t) {
        return t.getA() + "=" + t.getB().toString();
      }
    }).value(), "\n");
  }

  @Deprecated
  private static String mkString(Iterable<?> as, String sep) {
    final StringBuilder b = new StringBuilder();
    for (Iterator<?> i = as.iterator(); i.hasNext();) {
      b.append(i.next());
      if (i.hasNext()) {
        b.append(sep);
      }
    }
    return b.toString();
  }

  @Deprecated
  public static String mkString(Iterable<?> as, String sep, String pre, String post) {
    return pre + mkString(as, sep) + post;
  }
}
