/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.distribution.aws.s3.signer;

import org.opencastproject.mediapackage.MediaPackageSerializer;
import org.opencastproject.util.ConfigurationException;
import org.opencastproject.util.OsgiUtil;
import org.opencastproject.util.data.Option;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

/**
 * Implementation of a {@link MediaPackageSerializer} that will support presigned URL feature for a Mediapackage
 */
@Component(
    immediate = false,
    service = MediaPackageSerializer.class,
    property = {
        "service.pid=org.opencastproject.distribution.aws.s3.signer.PresignedUrlMediaPackageSerializer",
        "distribution.channel=aws.s3"
    }
)
public class PresignedUrlMediaPackageSerializer implements MediaPackageSerializer {

  private static final Logger logger = LoggerFactory.getLogger(PresignedUrlMediaPackageSerializer.class);

  public static final int RANKING = 10;

  public static final String AWS_S3_PRESIGNED_URL_CONFIG
      = "org.opencastproject.distribution.aws.s3.signer.presigned.url";
  public static final String AWS_S3_BUCKET_CONFIG = "org.opencastproject.distribution.aws.s3.signer.bucket";
  public static final String AWS_S3_REGION_CONFIG = "org.opencastproject.distribution.aws.s3.signer.region";
  public static final String AWS_S3_ENDPOINT_CONFIG = "org.opencastproject.distribution.aws.s3.signer.endpoint";
  public static final String AWS_S3_PATH_STYLE_CONFIG = "org.opencastproject.distribution.aws.s3.signer.path.style";
  public static final String AWS_S3_ACCESS_KEY_ID_CONFIG = "org.opencastproject.distribution.aws.s3.signer.access.id";
  public static final String AWS_S3_SECRET_ACCESS_KEY_CONFIG
      = "org.opencastproject.distribution.aws.s3.signer.secret.key";
  public static final String AWS_S3_PRESIGNED_URL_VALID_DURATION_CONFIG
      = "org.opencastproject.distribution.aws.s3.signer.presigned.url.valid.duration";

  // Defaults

  /** Default expiration time for presigned URL in millis, 6 hours */
  public static final int DEFAULT_PRESIGNED_URL_EXPIRE_MILLIS = 6 * 60 * 60 * 1000;

  /** Max expiration time for presigned URL in millis, 7 days */
  private static final int MAXIMUM_PRESIGNED_URL_EXPIRE_MILLIS = 7 * 24 * 60 * 60 * 1000;

  /** The AWS client and transfer manager */
  private AmazonS3 s3 = null;

  /** The AWS S3 bucket name */
  private String bucketName = null;

  /** The AWS S3 endpoint */
  private String endpoint = null;

  /** path style enabled */
  private boolean pathStyle = false;

  /** whether use presigned URL */
  private boolean presignedUrl = false;

  /** valid duration for presigned URL in milliseconds */
  private int presignedUrlValidDuration = DEFAULT_PRESIGNED_URL_EXPIRE_MILLIS;

  private String getAWSConfigKey(ComponentContext cc, String key) {
    try {
      return OsgiUtil.getComponentContextProperty(cc, key);
    } catch (RuntimeException e) {
      throw new ConfigurationException(key + " is missing or invalid", e);
    }
  }

  @Activate
  public void activate(ComponentContext cc) {

    // Get the configuration
    if (cc != null) {

      if (!BooleanUtils.toBoolean(getAWSConfigKey(cc, AWS_S3_PRESIGNED_URL_CONFIG))) {
        logger.info("AWS S3 distribution signer disabled");
        return;
      }

      // AWS S3 bucket name
      bucketName = getAWSConfigKey(cc, AWS_S3_BUCKET_CONFIG);
      logger.info("AWS S3 bucket name is {}", bucketName);

      // AWS region
      String regionStr = getAWSConfigKey(cc, AWS_S3_REGION_CONFIG);
      logger.info("AWS region is {}", regionStr);

      // AWS endpoint
      endpoint = OsgiUtil.getComponentContextProperty(cc, AWS_S3_ENDPOINT_CONFIG, "s3." + regionStr + ".amazonaws.com");
      logger.info("AWS S3 endpoint is {}", endpoint);

      // AWS path style
      pathStyle = BooleanUtils.toBoolean(OsgiUtil.getComponentContextProperty(cc, AWS_S3_PATH_STYLE_CONFIG, "false"));
      logger.info("AWS path style is {}", pathStyle);

      // AWS presigned URL
      String presignedUrlConfigValue = OsgiUtil.getComponentContextProperty(cc, AWS_S3_PRESIGNED_URL_CONFIG, "false");
      presignedUrl = StringUtils.equalsIgnoreCase("true", presignedUrlConfigValue);
      logger.info("AWS use presigned URL: {}", presignedUrl);

      // AWS presigned URL expiration time in millis
      String presignedUrlExpTimeMillisConfigValue = OsgiUtil.getComponentContextProperty(cc,
              AWS_S3_PRESIGNED_URL_VALID_DURATION_CONFIG, null);
      presignedUrlValidDuration = NumberUtils.toInt(presignedUrlExpTimeMillisConfigValue,
              DEFAULT_PRESIGNED_URL_EXPIRE_MILLIS);
      if (presignedUrlValidDuration > MAXIMUM_PRESIGNED_URL_EXPIRE_MILLIS) {
        logger.warn(
                "Valid duration of presigned URL is too large, MAXIMUM_PRESIGNED_URL_EXPIRE_MILLIS(7 days) is used");
        presignedUrlValidDuration = MAXIMUM_PRESIGNED_URL_EXPIRE_MILLIS;
      }

      // Explicit credentials are optional.
      AWSCredentialsProvider provider = null;
      Option<String> accessKeyIdOpt = OsgiUtil.getOptCfg(cc.getProperties(), AWS_S3_ACCESS_KEY_ID_CONFIG);
      Option<String> accessKeySecretOpt = OsgiUtil.getOptCfg(cc.getProperties(), AWS_S3_SECRET_ACCESS_KEY_CONFIG);

      // Keys not informed so use default credentials provider chain, which
      // will look at the environment variables, java system props, credential files, and instance
      // profile credentials
      if (accessKeyIdOpt.isNone() && accessKeySecretOpt.isNone()) {
        provider = new DefaultAWSCredentialsProviderChain();
      } else {
        provider = new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(accessKeyIdOpt.get(), accessKeySecretOpt.get()));
      }

      // S3 client configuration
      ClientConfiguration clientConfiguration = new ClientConfiguration();

      // Create AWS client
      s3 = AmazonS3ClientBuilder.standard()
              .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, regionStr))
              .withClientConfiguration(clientConfiguration)
              .withPathStyleAccessEnabled(pathStyle).withCredentials(provider).build();

      logger.info("AwsS3DistributionServiceSigner activated!");
    }
  }

  protected URI presignedURI(URI uri) throws URISyntaxException {
    if (!presignedUrl) {
      return uri;
    }
    String s3UrlPrefix = s3.getUrl(bucketName, "").toString();

    // Only handle URIs match s3 domain and bucket
    if (uri.toString().startsWith(s3UrlPrefix)) {
      String objectName = uri.toString().substring(s3UrlPrefix.length());
      Date validUntil = new Date(System.currentTimeMillis() + presignedUrlValidDuration);
      return s3.generatePresignedUrl(bucketName, objectName, validUntil).toURI();
    } else {
      return uri;
    }
  }

  /**
   * {@inheritDoc}
   *
   * Generate a presigned URI for the given URI if AwsS3DistributionService is enabled.
   */
  @Override
    public URI decodeURI(URI uri) throws URISyntaxException {
    URI presignedURI = presignedURI(uri);
    logger.debug("Decode in presigned URL serializer: {} -> {}", uri, presignedURI);
    return presignedURI;
  }

  /**
   * {@inheritDoc}
   */
  @Override
    public URI encodeURI(URI uri) throws URISyntaxException {
    URI encodedUri = null;
    logger.debug("Encode in presigned URL serializer: {} -> {}", uri, encodedUri);
    return uri;
  }

  /**
   * {@inheritDoc}
   */
  @Override
    public int getRanking() {
    return RANKING;
  }
}
