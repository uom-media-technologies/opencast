/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.opencastproject.pm.impl.OsgiEmailSenderService.parseMode;
import static org.opencastproject.util.data.Option.none;
import static org.opencastproject.util.data.Option.some;

import org.opencastproject.messages.MailService;
import org.opencastproject.messages.TemplateType;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabase;
import org.opencastproject.pm.impl.AbstractEmailSenderService.Mode;
import org.opencastproject.pm.impl.persistence.ParticipationManagementDatabaseImpl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.osgi.service.cm.ConfigurationException;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

@Ignore
public class OsgiEmailSenderServiceTest {
  private OsgiEmailSenderService osgiEmailSenderService;

  @Rule
  public final ExpectedException exception = ExpectedException.none();

  @Before
  public void setUp() {
    osgiEmailSenderService = new OsgiEmailSenderService();
  }

  @Test
  public void testUpdated() throws ConfigurationException {
    Dictionary<String, String> dictionary = new Hashtable<>();
    dictionary.put("opt-out-link", "opt-out-link");
    dictionary.put("mode", "smtp");
    osgiEmailSenderService.updated(dictionary);
    assertEquals("opt-out-link", osgiEmailSenderService.getOptOutLink("0"));
    assertTrue(osgiEmailSenderService.getMode().isUpdateRecordings());
    assertEquals("SMTP", osgiEmailSenderService.getMode().getType().toString());
  }

  @Test
  public void testUpdatedWithoutOptOutLink() throws ConfigurationException {
    Dictionary<String, String> dictionary = new Hashtable<>();
    assertEquals("", osgiEmailSenderService.getOptOutLink("0"));
    exception.expect(ConfigurationException.class);
    osgiEmailSenderService.updated(dictionary);
  }

  @Test
  public void testUpdatedWithoutDictionary() throws ConfigurationException {
    osgiEmailSenderService.updated(null);
    assertEquals("", osgiEmailSenderService.getOptOutLink("0"));
    assertFalse(osgiEmailSenderService.getMode().isUpdateRecordings());
    assertEquals("TEST_FILE", osgiEmailSenderService.getMode().getType().toString());
  }

  @Test
  public void testParseMode() {
    assertEquals(none(Mode.class), parseMode(""));
    assertEquals(none(Mode.class), parseMode("bla"));
    assertEquals(none(Mode.class), parseMode("test-smtp"));
    assertEquals(none(Mode.class), parseMode("test-smtp:::"));
    assertEquals(none(Mode.class), parseMode("test-smtp:::"));
    assertEquals(none(Mode.class), parseMode("test-smtp:root@localhost::"));
    assertEquals(none(Mode.class), parseMode("test-smtp:root@localhost:100a:"));
    assertEquals(none(Mode.class), parseMode("test-smtp:root@localhost:100a:false"));
    assertEquals(none(Mode.class), parseMode("test:root@localhost:100:false"));
    assertEquals(some(new Mode.TestSmtpMode("root@localhost", 100, false)),
            parseMode("test-smtp:root@localhost:100:false"));
    assertEquals(some(new Mode.SmtpMode()), parseMode("smtp"));
    assertEquals(some(new Mode.TestFileMode("/dev/null", -1, true)),
            parseMode("test-file:/dev/null:none:true"));
  }

  @Test
  public void testRenderInvitationBody() {
    List<TemplateType.Invitation.Module> modules = new ArrayList<>();
    TemplateType.Invitation.Data data = new TemplateType.Invitation.Data("", "", modules);
    assertEquals("", osgiEmailSenderService.renderInvitationBody("", data));
  }

  @Test
  public void testGetDb() {
    ParticipationManagementDatabase db = osgiEmailSenderService.getDb();
    assertNull(db);

    ParticipationManagementDatabase newDb = new ParticipationManagementDatabaseImpl();
    osgiEmailSenderService.setDb(newDb);
    assertNotNull(newDb);
  }

  @Test
  public void testGetMailService() {
    MailService mailService = osgiEmailSenderService.getMailService();
    assertNull(mailService);

    osgiEmailSenderService.setMailService(new MailService());
    mailService = osgiEmailSenderService.getMailService();
    assertNotNull(mailService);
  }
}
