/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.impl.scheduling;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.opencastproject.metadata.dublincore.DublinCore;
import org.opencastproject.metadata.dublincore.DublinCoreCatalog;
import org.opencastproject.pm.api.CaptureAgent;
import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.Person;
import org.opencastproject.pm.api.PersonType;
import org.opencastproject.pm.api.Recording;
import org.opencastproject.pm.api.Room;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabase;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabaseException;
import org.opencastproject.pm.api.scheduling.ParticipationManagementSchedulingException;
import org.opencastproject.pm.api.scheduling.Schedule;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.JaxbUser;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.User;
import org.opencastproject.security.api.UserDirectoryService;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Option;
import org.opencastproject.util.data.VCell;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import org.easymock.EasyMock;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ParticipationManagementProviderTest {
  private ComboPooledDataSource pooledDataSource = null;
  private ParticipationManagementDatabase pmDB = null;
  private ParticipationManagementProvider pmProvider = null;
  private SeriesService seriesService = null;
  private SecurityService securityService = null;
  private Date upcomingDate = new DateTime().plusHours(4).toDate();

  @Before
  public void setUp() throws Exception {
    // Set up the database
    pooledDataSource = new ComboPooledDataSource();
    pooledDataSource.setDriverClass("org.h2.Driver");
    pooledDataSource.setJdbcUrl("jdbc:h2:./target/db" + System.currentTimeMillis());
    pooledDataSource.setUser("sa");
    pooledDataSource.setPassword("sa");

    // Set up the persistence properties
    Map<String, Object> props = new HashMap<>();
    props.put("javax.persistence.nonJtaDataSource", pooledDataSource);
    props.put("eclipselink.ddl-generation", "create-tables");
    props.put("eclipselink.ddl-generation.output-mode", "database");

    User user = new JaxbUser(
           "test",
           null,
           "Test User",
           "test@test.com",
           "test",
           new DefaultOrganization(),
           new HashSet<>());

    SecurityService securityService = createNiceMock(SecurityService.class);
    expect(securityService.getOrganization()).andReturn(new DefaultOrganization()).anyTimes();
    replay(securityService);

    UserDirectoryService userDirectoryService = createNiceMock(UserDirectoryService.class);
    expect(userDirectoryService.loadUser(EasyMock.anyObject())).andReturn(user).anyTimes();
    replay(userDirectoryService);

    // Set up the annotation service
    pmDB = createNiceMock(ParticipationManagementDatabase.class);
    seriesService = createNiceMock(SeriesService.class);
    pmProvider = new ParticipationManagementProvider(
           pmDB,
           seriesService,
           securityService,
           true,
           VCell.cell(0),
           VCell.cell(0));

    replay(pmDB);
    replay(seriesService);

    verify(pmDB);
    verify(securityService);
    verify(userDirectoryService);
  }

  @After
  public void tearDown() throws ParticipationManagementDatabaseException {
//    boolean clearedDatabase = pmDB.clearDatabase();
//    assertEquals(clearedDatabase, true);
  }

  @Ignore
  @Test
  public void testRecordingWithCourseAndWithoutSeries() {
    Recording recording = createRecording();
    final String seriesId = recording.getCourse().get().getCourseId().replaceAll(" ", "-");
    DublinCoreCatalog dc = ParticipationManagementProvider.toSeriesCatalog(recording.getCourse().get());

    try {
      expect(seriesService.getSeries(seriesId)).andReturn(dc).anyTimes();
      expect(seriesService.getSeries(EasyMock.anyObject())).andThrow(new NotFoundException()).anyTimes();
      expect(seriesService.updateSeries(EasyMock.anyObject())).andReturn(dc).anyTimes();
      expect(seriesService.updateAccessControl(EasyMock.anyObject(), EasyMock.anyObject())).andReturn(true).once();
      expect(pmDB.updateRecording(recording)).andStubReturn(recording);
      replay(seriesService);
    } catch (Exception e) {
      Assert.fail("Exception when describing mock behavior ");
    }

    assertEquals(dc.getFirst(DublinCore.PROPERTY_TITLE), "Math 2015/16");

    try {
      recording = pmDB.updateRecording(recording);
    } catch (ParticipationManagementDatabaseException e) {
      Assert.fail("Can not update recording");
    }

    // With course but no series (a new one is created)
    try {
      Schedule schedule = pmProvider.fetchSchedule(new Date(), upcomingDate);
      assertEquals(1, schedule.getEpisodes().size());
      Recording result = schedule.getEpisodes().get(0).getA();
      assertEquals(recording.getId(), result.getId());
      Assert.assertFalse(recording.getCourse().get().equals(result.getCourse().get()));
      assertEquals("I1234-MATH-56789-1151-1YR-012345", recording.getCourse().get().getExternalCourseKey());
    } catch (ParticipationManagementSchedulingException e) {
      Assert.fail("Cannot fetch recording: " + e.getMessage());
      e.printStackTrace();
    }

    EasyMock.verify(seriesService);
  }

  @Ignore
  @Test
  public void testRecordingWithCourseAndWithoutSeriesNoCreation() {
    pmProvider = new ParticipationManagementProvider(
           pmDB, seriesService, securityService, false, VCell.cell(0), VCell.cell(0));

    Recording recording = createRecording();
    final String seriesId = recording.getCourse().get().getCourseId().replaceAll(" ", "-");
    DublinCoreCatalog dc = ParticipationManagementProvider.toSeriesCatalog(recording.getCourse().get());

    try {
      expect(seriesService.getSeries(seriesId)).andReturn(dc).anyTimes();
      expect(seriesService.getSeries(EasyMock.anyObject())).andThrow(new NotFoundException()).anyTimes();
      expect(seriesService.updateSeries(EasyMock.anyObject())).andReturn(dc).anyTimes();
      replay(seriesService);
    } catch (Exception e) {
      Assert.fail("Exception when describing mock behavior ");
    }

    try {
      recording = pmDB.updateRecording(recording);
    } catch (ParticipationManagementDatabaseException e) {
      Assert.fail("Can not update recording");
    }

    try {
      pmProvider.fetchSchedule(new Date(), upcomingDate);
    } catch (ParticipationManagementSchedulingException e) {
      Assert.assertTrue(true);
    }
  }

  @Ignore
  @Test
  public void testRecordingWithCourseAndSeries() {
    Recording recording = createRecording();
    final String seriesId = recording.getCourse().get().getSeriesId();
    DublinCoreCatalog dc = ParticipationManagementProvider.toSeriesCatalog(recording.getCourse().get());

    try {
      expect(seriesService.getSeries(seriesId)).andReturn(dc).once();
      expect(seriesService.updateSeries(EasyMock.anyObject())).andReturn(dc).anyTimes();
      expect(seriesService.updateAccessControl(EasyMock.anyObject(), EasyMock.anyObject())).andReturn(true).once();
      replay(seriesService);
    } catch (Exception e) {
      Assert.fail("Exception when describing mock behavior ");
    }

    try {
      recording = pmDB.updateRecording(recording);
    } catch (ParticipationManagementDatabaseException e) {
      Assert.fail("Can not update recording");
    }

    // With course but no series (a new one is created)
    try {
      Schedule schedule = pmProvider.fetchSchedule(new Date(), upcomingDate);
      assertEquals(1, schedule.getEpisodes().size());
      Recording result = schedule.getEpisodes().get(0).getA();
      assertEquals(recording.getId(), result.getId());
      assertEquals(recording.getCourse().get(), result.getCourse().get());
    } catch (ParticipationManagementSchedulingException e) {
      Assert.fail("Can not fetch recording: " + e.getMessage());
      e.printStackTrace();
    }

    EasyMock.verify(seriesService);
  }

  @Ignore
  @Test
  public void testRecordingWithoutCourse() {
    Recording recording = createRecording();
    recording.setCourse(Option.none(Course.class));

    try {
      recording = pmDB.updateRecording(recording);
    } catch (ParticipationManagementDatabaseException e) {
      Assert.fail("Can not update recording");
    }

    // With course but no series (a new one is created)
    try {
      Schedule schedule = pmProvider.fetchSchedule(new Date(), upcomingDate);
      assertEquals(1, schedule.getEpisodes().size());
      Recording result = schedule.getEpisodes().get(0).getA();
      assertEquals(recording.getId(), result.getId());
      Assert.assertTrue(result.getCourse().isNone());
    } catch (ParticipationManagementSchedulingException e) {
      Assert.fail("Can not fetch recording: " + e.getMessage());
      e.printStackTrace();
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFetchScheduleWithoutFromDate() throws ParticipationManagementSchedulingException {
    pmProvider.fetchSchedule(null, new Date());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testFetchScheduleWithoutUntilDate() throws ParticipationManagementSchedulingException {
    pmProvider.fetchSchedule(new Date(), null);
  }
  private Recording createRecording() {
    List<Person> staff = new ArrayList<>();
    List<PersonType> staffTypes = new ArrayList<>();
    staffTypes.add(new PersonType("group", "open door"));
    staff.add(Person.person("Staff 1", "staff1@university.org", staffTypes));
    staff.add(Person.person("Staff 2", "staff2@university.org", staffTypes));

    List<Person> students = new ArrayList<>();
    List<PersonType> studentTypes = new ArrayList<PersonType>();
    studentTypes.add(new PersonType("student", "learning"));
    students.add(Person.person("Student 1", "student1@university.org", studentTypes));
    students.add(Person.person("Student 2", "student2@university.org", studentTypes));

    Room room = new Room("Aula");
    CaptureAgent captureAgent = new CaptureAgent(room, "mh_ncast1", "screen");
    Course course = new Course(
            "mathe",
            "uuid",
            "Math",
            "Simple course about algebra.",
            "I1234-MATH-56789-1151-1YR-012345");

    return Recording.recording(
            "activityId",
            "Test title",
            false,
            staff,
            Option.some(course),
            room,
            new Date(),
            new DateTime().plusHours(2).toDate(),
            new DateTime().plusHours(3).toDate(),
            students,
            new ArrayList<>(),
            Option.some("0"),
            captureAgent,
            new ArrayList<>(),
            Course.EmailStatus.UNSENT,
            Recording.ReviewStatus.UNCONFIRMED,
            Option.some(new Date()),
            false,
            false,
            "screen");
  }
}
