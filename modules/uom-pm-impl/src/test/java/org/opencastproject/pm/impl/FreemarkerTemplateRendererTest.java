/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.impl;

import static org.junit.Assert.assertEquals;
import static org.opencastproject.util.data.Collections.list;
import static org.opencastproject.util.data.Collections.map;
import static org.opencastproject.util.data.Tuple.tuple;

import org.opencastproject.messages.TemplateType;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class FreemarkerTemplateRendererTest {

  private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

  @Before
  public void setUp() {
    freemarkerTemplateRenderer = new FreemarkerTemplateRenderer();
  }

  @Test
  public void testRenderEmptyString() {
    String result = freemarkerTemplateRenderer.render("", null);

    assertEquals("", result);
  }

  @Test
  public void testRenderEmptyStringEmptyMap() {
    Map<String, String> map = new HashMap<>();
    String result = freemarkerTemplateRenderer.render("", map);

    assertEquals("", result);
  }

  @Test
  public void testRenderEmptyStringMap() {
    Map<String, String> map = new HashMap<>();
    map.put("key", "value");
    String result = freemarkerTemplateRenderer.render("", map);

    assertEquals("", result);
  }

  @Test
  public void testRenderString() {
    String result = freemarkerTemplateRenderer.render("template", null);

    assertEquals("template", result);
  }

  @Test
  public void testRenderStringEmptyMap() {
    Map<String, String> map = new HashMap<>();
    String result = freemarkerTemplateRenderer.render("template", map);

    assertEquals("template", result);
  }

  @Test
  public void testRenderStringMap() {
    Map<String, String> map = new HashMap<>();
    map.put("key", "value");
    String result = freemarkerTemplateRenderer.render("template", map);

    assertEquals("template", result);
  }

  @Test
  public void testRenderInvitationMap() throws Exception {
    final FreemarkerTemplateRenderer r = new FreemarkerTemplateRenderer();
    final String template = loadTxtFromClassPath("mail-template-invitation.ftl", getClass());
    final String rendered = r.render(template, map(tuple("staff", "Dr. Lurch"), tuple("modules",
                    list(map(tuple("name", "Module 1"), tuple("description", "This is module no. 1")),
                            map(tuple("name", "Module 2"), tuple("description", "This is module no. 2")))),
            tuple("optOutLink", "http://manchester.uk.ac/pmm/optout")));

    assertEquals(loadTxtFromClassPath("mail-template-invitation-rendered.txt", getClass()), rendered);
  }

  @Test
  public void testRenderInvitationData() throws Exception {
    final FreemarkerTemplateRenderer r = new FreemarkerTemplateRenderer();
    final String template = loadTxtFromClassPath("mail-template-invitation.ftl", getClass());
    final String rendered = r.render(template,
            TemplateType.Invitation.data("Dr. Lurch", "http://manchester.uk.ac/pmm/optout",
                    list(TemplateType.Invitation.module("Module 1", "This is module no. 1", 0, false, false, false),
                            TemplateType.Invitation.module("Module 2", "This is module no. 2", 0, false, false,
                                    false))));

    assertEquals(loadTxtFromClassPath("mail-template-invitation-rendered.txt", getClass()), rendered);
  }

  @Test
  public void testRenderInvitationDataDescriptionMissing() throws Exception {
    final FreemarkerTemplateRenderer r = new FreemarkerTemplateRenderer();
    final String template = loadTxtFromClassPath("mail-template-invitation.ftl", getClass());
    final String rendered = r.render(template,
            TemplateType.Invitation.data("Colleague", "http://manchester.uk.ac/pmm/optout",
                    list(TemplateType.Invitation.module("Module 1", "This is module no. 1", 0, false, false, false),
                            TemplateType.Invitation.module("Module 2", "", 0, false, false, false))));
    assertEquals(
        loadTxtFromClassPath("mail-template-invitation-rendered-description-missing.txt", getClass()), rendered);
  }

  @Test
  public void testRenderInvitationDataDescriptionEmpty() {
    final FreemarkerTemplateRenderer r = new FreemarkerTemplateRenderer();
    final String template = loadTxtFromClassPath("mail-template-invitation.ftl", getClass());
    final TemplateType.Invitation.Module module1 = TemplateType.Invitation.module("Module 1", "This is module no. 1", 0,
            false, false, false);
    final TemplateType.Invitation.Module module2 = TemplateType.Invitation.module("Module 2", "", 0, false, false,
            false);
    final String rendered = r.render(template,
            TemplateType.Invitation.data("Dr. Lurch", "http://manchester.uk.ac/pmm/optout", list(module1, module2)));
    assertEquals(
        loadTxtFromClassPath("mail-template-invitation-rendered-description-missing.txt", getClass()), rendered);
  }

  public String loadTxtFromClassPath(String filename, Class clazz) {
    try {
      return IOUtils.toString(this.getClass().getResourceAsStream(filename), StandardCharsets.UTF_8);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }
}
