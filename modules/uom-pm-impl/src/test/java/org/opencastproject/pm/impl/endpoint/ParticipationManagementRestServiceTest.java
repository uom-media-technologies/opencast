/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.impl.endpoint;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.opencastproject.kernel.security.SecurityServiceSpringImpl;
import org.opencastproject.pm.api.CaptureAgent;
import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.Person;
import org.opencastproject.pm.api.Recording;
import org.opencastproject.pm.api.Room;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabaseException;
import org.opencastproject.pm.api.scheduling.ParticipationFeederService;
import org.opencastproject.pm.impl.persistence.ParticipationManagementDatabaseImpl;
import org.opencastproject.pm.impl.scheduling.ScheduleFeederServiceImpl;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.NotFoundException;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response;

public class ParticipationManagementRestServiceTest {
  private ParticipationManagementRestService participationManagementRestService;
  private ParticipationFeederService feederService;

  @Before
  public void setUp() {
    participationManagementRestService = new ParticipationManagementRestService();
    SecurityService securityService = new SecurityServiceSpringImpl();
    ParticipationManagementDatabaseImpl database = new ParticipationManagementDatabaseImpl();
    database.setSecurityService(securityService);

    SeriesService seriesService = createMock(SeriesService.class);

    participationManagementRestService = new ParticipationManagementRestService();
    participationManagementRestService.setPersistence(database);
    participationManagementRestService.setSecurityService(securityService);
    participationManagementRestService.setScheduleFeederService(new ScheduleFeederServiceImpl());
    participationManagementRestService.setSeriesService(seriesService);

    feederService = createMock(ParticipationFeederService.class);

    participationManagementRestService.setFeederService(feederService);
  }

  @Test
  public void testActivitySeriesId503() throws Exception {
    participationManagementRestService.setFeederService(null);

    Response response = participationManagementRestService.activitySeriesId(null);
    assertEquals(503, response.getStatus());
  }

  @Test
  public void testActivitySeriesId200() throws Exception {
    expect(feederService.getCourseByActivityId("0")).andStubReturn(new Course("0"));
    replay(feederService);

    Response response = participationManagementRestService.activitySeriesId("0");
    assertEquals(201, response.getStatus());

    verify(feederService);
  }

  @Test
  public void testModuleSeriesId503() throws Exception {
    participationManagementRestService.setFeederService(null);

    Response response = participationManagementRestService.moduleSeriesId(null);
    assertEquals(503, response.getStatus());
  }

  @Test
  public void testModuleSeriesId200() throws Exception {
    List<String> courses = new ArrayList<>();
    courses.add("0");

    List<String> activities = new ArrayList<>();
    activities.add("0");

    expect(feederService.getActivityIds("0")).andStubReturn(activities);
    expect(feederService.getCourseByCourseKeys(courses)).andStubReturn(new Course("0"));
    expect(feederService.getCourseByActivityId("0")).andStubReturn(new Course("0"));
    replay(feederService);

    Response response = participationManagementRestService.moduleSeriesId("0");
    assertEquals(200, response.getStatus());

    verify(feederService);
  }

  @Test
  public void testGetOptoutId() throws Exception {
    Response response = participationManagementRestService.optoutID("");
    assertEquals(200, response.getStatus());
  }

  @Test
  public void testCreateCourse() throws Exception {
    participationManagementRestService.setFeederService(createNiceMock(ParticipationFeederService.class));
    participationManagementRestService.setSeriesService(createNiceMock(SeriesService.class));

    Response response = participationManagementRestService.createCourse("1", "1");
    assertEquals(200, response.getStatus());
  }

  @Test
  public void testModifyRecordingStatus400() throws NotFoundException {
    Response response = participationManagementRestService.modifyRecordingStatus(0L, "");
    assertEquals(400, response.getStatus());
  }

  @Test
  public void testModifyRecordingStatusOptOut() throws NotFoundException, ParticipationManagementDatabaseException {
    Recording recording = Recording.recording("", "", new ArrayList<>(), new Course("0"), new Room("0"),
            new Date(), new Date(), new Date(), new ArrayList<Person>(), new CaptureAgent(new Room("0"), "0", "0"));
    ParticipationManagementDatabaseImpl database = createMockBuilder(
            ParticipationManagementDatabaseImpl.class).addMockedMethod("reviewRecording")
            .addMockedMethod("trimRecording").createNiceMock();
    expect(database.reviewRecording(0L, Recording.ReviewStatus.OPTED_OUT)).andReturn(recording);
    expect(database.trimRecording(0L, false)).andReturn(recording);
    replay(database);
    participationManagementRestService.setPersistence(database);
    Response response = participationManagementRestService.modifyRecordingStatus(0L, "OPT_OUT");
    assertEquals(200, response.getStatus());
  }

  @Test
  public void testModifyRecordingStatusNoTrim() throws NotFoundException, ParticipationManagementDatabaseException {
    Recording recording = Recording.recording("", "", new ArrayList<>(), new Course("0"), new Room("0"),
            new Date(), new Date(), new Date(), new ArrayList<Person>(), new CaptureAgent(new Room("0"), "0", "0"));
    ParticipationManagementDatabaseImpl database = createMockBuilder(
            ParticipationManagementDatabaseImpl.class).addMockedMethod("reviewRecording")
            .addMockedMethod("trimRecording").createNiceMock();
    expect(database.reviewRecording(0L, Recording.ReviewStatus.OPTED_OUT)).andReturn(recording);
    expect(database.trimRecording(0L, false)).andReturn(recording);
    replay(database);
    participationManagementRestService.setPersistence(database);
    Response response = participationManagementRestService.modifyRecordingStatus(0L, "OPT_OUT");
    assertEquals(200, response.getStatus());
  }

  @Test
  public void testModifyRecordingStatusTrim() throws NotFoundException, ParticipationManagementDatabaseException {
    Recording recording = Recording.recording("", "", new ArrayList<>(), new Course("0"), new Room("0"),
            new Date(), new Date(), new Date(), new ArrayList<Person>(), new CaptureAgent(new Room("0"), "0", "0"));
    ParticipationManagementDatabaseImpl database = createMockBuilder(
            ParticipationManagementDatabaseImpl.class).addMockedMethod("reviewRecording")
            .addMockedMethod("trimRecording").createNiceMock();
    expect(database.reviewRecording(0L, Recording.ReviewStatus.OPTED_OUT)).andReturn(recording);
    expect(database.trimRecording(0L, true)).andReturn(recording);
    replay(database);
    participationManagementRestService.setPersistence(database);
    Response response = participationManagementRestService.modifyRecordingStatus(0L, "OPT_OUT");
    assertEquals(200, response.getStatus());
  }
}
