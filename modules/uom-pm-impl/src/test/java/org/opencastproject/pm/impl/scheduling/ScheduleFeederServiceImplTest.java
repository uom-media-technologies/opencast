/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.impl.scheduling;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;

import org.opencastproject.pm.api.CaptureAgent;
import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.Recording;
import org.opencastproject.pm.api.Room;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabase;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabaseException;
import org.opencastproject.pm.api.persistence.RecordingQuery;
import org.opencastproject.pm.api.scheduling.ParticipationManagementSchedulingException;
import org.opencastproject.security.api.JaxbOrganization;
import org.opencastproject.security.api.JaxbRole;
import org.opencastproject.security.api.JaxbUser;
import org.opencastproject.security.api.OrganizationDirectoryService;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.UnauthorizedException;
import org.opencastproject.security.api.User;
import org.opencastproject.security.util.SecurityContext;
import org.opencastproject.security.util.SecurityUtil;
import org.opencastproject.series.api.SeriesException;
import org.opencastproject.series.api.SeriesService;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Option;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.osgi.service.cm.ConfigurationException;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Test cases for class {@link ScheduleFeederServiceImpl}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityUtil.class)
public class ScheduleFeederServiceImplTest {
  private ParticipationManagementDatabase database;
  private ScheduleFeederServiceImpl scheduleFeederService;

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Before
  public void setUp() throws NotFoundException {
    database = createNiceMock(ParticipationManagementDatabase.class);
    OrganizationDirectoryService organizationDirectoryService = createNiceMock(OrganizationDirectoryService.class);
    SecurityService securityService = createNiceMock(SecurityService.class);
    mockStatic(SecurityUtil.class);

    expect(organizationDirectoryService.getOrganization("value")).andStubReturn(new JaxbOrganization());
    expect(SecurityUtil.createSystemUser("value", new JaxbOrganization())).andStubReturn(new JaxbUser());

    replay(organizationDirectoryService);
    PowerMock.replay(SecurityUtil.class);

    scheduleFeederService = new ScheduleFeederServiceImpl();
    scheduleFeederService.setParticipationManagementDatabase(database);
    scheduleFeederService.setOrganizationDirectoryService(organizationDirectoryService);
    scheduleFeederService.setSecurityService(securityService);

    verify(organizationDirectoryService);
    PowerMock.verify(SecurityUtil.class);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testUpdatedWithoutProperties() throws ConfigurationException {
    scheduleFeederService.updated(null);
  }

  @Test(expected = ConfigurationException.class)
  public void testUpdatedEmptyProperties() throws ConfigurationException {
    Dictionary properties = new Hashtable<>();
    scheduleFeederService.updated(properties);
  }

  @Test
  public void testUpdatedWithUpcomingDaysException() throws ConfigurationException {
    Dictionary properties = new Hashtable<>();
    properties.put("cron", "value");
    properties.put("workflow.definition", "value");
    properties.put("organization", "value");
    properties.put("start-margin", "value");
    properties.put("end-margin", "value");
    properties.put("workflow.property.edit", "value");
    properties.put("workflow.property.email", "value");
    properties.put("capture.room.__ANY__.inputs", "value");
    properties.put("capture.device.name.input.value", "value");
    properties.put("upcoming.days", "value");
    expectedException.expect(ConfigurationException.class);
    expectedException.expectMessage("upcoming.days : For input string: \"value\"");
    scheduleFeederService.updated(properties);
  }

//  @PrepareForTest(SecurityUtil.class)
//  @Test
  public void testUpdatedWithUpcomingDays() throws ConfigurationException {
    Dictionary properties = new Hashtable<>();
    properties.put("cron", "value");
    properties.put("workflow.definition", "value");
    properties.put("organization", "org");
    properties.put("start-margin", "value");
    properties.put("end-margin", "value");
    properties.put("workflow.property.edit", "value");
    properties.put("workflow.property.email", "value");
    properties.put("capture.room.__ANY__.inputs", "value");
    properties.put("capture.device.name.input.value", "value");
    properties.put("upcoming.days", "1");

//    reset(SecurityUtil.class, SecurityService.class);
//    createMock(SecurityUtil.class);
    mockStatic(SecurityUtil.class);
    SecurityService securityService = createNiceMock(SecurityService.class);

    JaxbOrganization org = new JaxbOrganization("org", "org", new HashMap<>(), "role", "role", new HashMap<>());
    JaxbRole role = new JaxbRole("role", org);
    User user = new JaxbUser("userName", "", org, role);
//    String PROPERTY_KEY_SYS_USER = "org.opencastproject.security.digest.user";
//    expect(context);
//    expect(SecurityUtil.createSystemUser("userName", org)).andReturn(user);
    expect(SecurityUtil.createSystemUser(user.getUsername(), org)).andReturn(user);
    expect(new SecurityContext(securityService, org, user)).andAnswer(() ->
            new SecurityContext(securityService, org, user));
//    replay(SecurityUtil.class, SecurityService.class);
    scheduleFeederService.updated(properties);
    verify(SecurityUtil.class, SecurityService.class);
  }

  public void testClearScheduledEvents()
          throws ParticipationManagementDatabaseException, ParticipationManagementSchedulingException {
    final AtomicBoolean functionCalled = new AtomicBoolean();
    functionCalled.set(false);

    List<Recording> recordings = new ArrayList<>();
    Room room = new Room("room");
    Course course = new Course("0");
    Recording recording = new Recording(
            Option.some(0L),
            "",
            Option.some("0"),
            "",
            new ArrayList<>(),
            Option.some(course),
            room,
            new Date(),
            false,
            false,
            new Date(),
            new Date(),
            Course.EmailStatus.SENT,
            Recording.ReviewStatus.CONFIRMED,
            Option.some(new Date()),
            new ArrayList<>(),
            new ArrayList<>(),
            new CaptureAgent(room, "0", "0"),
            new ArrayList<>(),
            Option.some(""),
            false,
            "0");
    recordings.add(recording);
    RecordingQuery query = RecordingQuery.create();
    query.withCourse(course);

    expect(database.findRecordings(query)).andStubReturn(recordings);
//    expect(database.findRecordings(RecordingQuery.create())).andAnswer(() -> {
//      functionCalled.set(true);
//      return recordings;
//    });
    replay(database);
    scheduleFeederService.clearScheduledEvents();
    verify(database);

//    assertTrue(functionCalled.get());

    reset(database);
  }

  @Test
  public void testClearSeriesEmptyList()
          throws ParticipationManagementSchedulingException, ParticipationManagementDatabaseException {
    final AtomicBoolean functionCalled = new AtomicBoolean();
    functionCalled.set(false);

    expect(database.getCourses()).andAnswer(() -> {
      functionCalled.set(true);
      return new ArrayList<>();
    });
    replay(database);
    scheduleFeederService.clearSeries();
    verify(database);

    assertTrue(functionCalled.get());

    reset(database);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testClearSeriesEmptyString()
          throws ParticipationManagementSchedulingException, ParticipationManagementDatabaseException {
    final AtomicBoolean functionCalled = new AtomicBoolean();
    functionCalled.set(false);

    expect(database.getCourses()).andAnswer(() -> {
      functionCalled.set(true);
      List<Course> series = new ArrayList<>();
      series.add(new Course(""));
      return series;
    });
    replay(database);
    scheduleFeederService.clearSeries();
    verify(database);

    assertTrue(functionCalled.get());

    reset(database);
  }

  @Test
  public void testClearSeriesSeriesIdNull()
          throws ParticipationManagementSchedulingException, ParticipationManagementDatabaseException {
    final AtomicBoolean functionCalled = new AtomicBoolean();
    functionCalled.set(false);

    expect(database.getCourses()).andAnswer(() -> {
      functionCalled.set(true);
      List<Course> series = new ArrayList<>();
      series.add(new Course("0"));
      return series;
    });
    replay(database);
    scheduleFeederService.clearSeries();
    verify(database);

    assertTrue(functionCalled.get());

    reset(database);
  }

  @Test
  public void testClearSeries()
          throws ParticipationManagementSchedulingException, ParticipationManagementDatabaseException,
                 UnauthorizedException, NotFoundException, SeriesException {
    final AtomicBoolean functionCalled = new AtomicBoolean();
    functionCalled.set(false);

    SeriesService seriesService = createNiceMock(SeriesService.class);
    scheduleFeederService.setSeriesService(seriesService);

    List<Course> series = new ArrayList<>();
    Course course = new Course("0");
    course.setSeriesId(course.createSeriesId());
    series.add(course);

    expect(database.getCourses()).andAnswer(() -> {
      functionCalled.set(true);
      return series;
    });
    replay(database);
    seriesService.deleteSeries(course.getSeriesId());
    scheduleFeederService.clearSeries();
    verify(database);

    assertTrue(functionCalled.get());
    assertEquals(0, seriesService.getSeriesCount());

    reset(database);
  }

  @Test
  public void testIsUpcoming() {
    scheduleFeederService.upcomingIntervalInDays = -1;
    assertTrue(scheduleFeederService.isUpcoming(new Date()));
    assertTrue(scheduleFeederService.isUpcoming(new Date(-1L)));
    assertTrue(scheduleFeederService.isUpcoming(new Date(0L)));
    assertTrue(scheduleFeederService.isUpcoming(new Date(1L)));

    scheduleFeederService.upcomingIntervalInDays = 0;
    assertTrue(scheduleFeederService.isUpcoming(new Date()));
    assertTrue(scheduleFeederService.isUpcoming(new Date(-1L)));
    assertTrue(scheduleFeederService.isUpcoming(new Date(0L)));
    assertTrue(scheduleFeederService.isUpcoming(new Date(1L)));

    scheduleFeederService.upcomingIntervalInDays = 1;
    assertTrue(scheduleFeederService.isUpcoming(new Date()));
    assertTrue(scheduleFeederService.isUpcoming(new Date(-1L)));
    assertTrue(scheduleFeederService.isUpcoming(new Date(0L)));
    assertTrue(scheduleFeederService.isUpcoming(new Date(1L)));
  }

  @Test
  public void testGetSynchronizationStartDate() {
    assertEquals(new Date(), scheduleFeederService.getSynchronizationStartDate());
  }

  @Test
  public void testGetSynchronizationStartDateSyncPast() {
    Date dateTime = new Date(0L);
    scheduleFeederService.syncPast = true;
    assertEquals(dateTime, scheduleFeederService.getSynchronizationStartDate());
  }

  @Test
  public void testGetSynchronizationEndDate() {
    scheduleFeederService.upcomingIntervalInDays = 1;
    scheduleFeederService.roundUpcoming = false;

    Date date = scheduleFeederService.getSynchronizationEndDate();

    assertTrue(Date.from(Instant.now()).before(date));
  }

  @Test
  public void testGetSynchronizationEndDateRoundUpcoming() {
    scheduleFeederService.upcomingIntervalInDays = 1;
    scheduleFeederService.roundUpcoming = true;

    Date date = scheduleFeederService.getSynchronizationEndDate();

    assertTrue(Date.from(Instant.now()).before(date));
  }

  @Test
  public void testGetSynchronizationEndDateNull() {
    assertNull(scheduleFeederService.getSynchronizationEndDate());

    scheduleFeederService.upcomingIntervalInDays = 0;
    assertNull(scheduleFeederService.getSynchronizationEndDate());

    scheduleFeederService.upcomingIntervalInDays = 1;
    assertNotNull(scheduleFeederService.getSynchronizationEndDate());
  }

  @Test
  public void testGetWfCfgAsMap() throws ConfigurationException {
    Dictionary<String, String> dictionary = new Hashtable<>();
    dictionary.put("key-1", "value-1");
    Map<String, String> map = ScheduleFeederServiceImpl.getWfCfgAsMap(dictionary, "key");
    assertEquals(1, map.size());
    assertNull(map.get("value"));
  }

  @Test
  public void testGetWfCfgAsMapReturnEmptyMap() throws ConfigurationException {
    Dictionary<String, String> dictionary = new Hashtable<>();
    dictionary.put("key", "value");
    Map<String, String> map = ScheduleFeederServiceImpl.getWfCfgAsMap(dictionary, "value");
    assertEquals(0, map.size());
    assertNull(map.get("value"));
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testGetWfCfgAsMapIndexOutOfBoundsException() throws ConfigurationException {
    Dictionary<String, String> dictionary = new Hashtable<>();
    dictionary.put("key", "value");
    Map<String, String> map = ScheduleFeederServiceImpl.getWfCfgAsMap(dictionary, "key");
  }

  @Test
  public void testGetWfCfgAsMapEmpty() throws ConfigurationException {
    Map<String, String> map = ScheduleFeederServiceImpl.getWfCfgAsMap(new Hashtable<>(), "");
    assertEquals(0, map.size());
  }
}
