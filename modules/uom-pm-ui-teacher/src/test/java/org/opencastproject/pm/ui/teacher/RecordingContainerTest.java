/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.ui.teacher;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import org.opencastproject.event.comment.EventCommentServiceImpl;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageImpl;
import org.opencastproject.mediapackage.identifier.IdImpl;
import org.opencastproject.pm.api.Action;
import org.opencastproject.pm.api.CaptureAgent;
import org.opencastproject.pm.api.Course;
import org.opencastproject.pm.api.Message;
import org.opencastproject.pm.api.Person;
import org.opencastproject.pm.api.PersonType;
import org.opencastproject.pm.api.Recording;
import org.opencastproject.pm.api.Room;
import org.opencastproject.pm.api.SchedulingSource;
import org.opencastproject.pm.api.persistence.ParticipationManagementDatabase;
import org.opencastproject.pm.api.persistence.RecordingQuery;
import org.opencastproject.util.data.Option;
import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowDefinitionImpl;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowService;

import org.junit.Ignore;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

@Ignore
public class RecordingContainerTest extends TestCase {
  private RecordingContainer recordingContainer;
  private WorkflowService workflowService;
  private WorkflowDefinition workflowDefinition;
  private WorkflowInstance workflowInstance;
  private MediaPackage mediaPackage;
  private Map<String, String> options;
  private ParticipationManagementDatabase database;

  public void setUp() throws Exception {
    super.setUp();

    String workflow = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
            .append("<definition xmlns=\"http://workflow.opencastproject.org\">")
            .append("</definition>")
            .toString();

    workflowService = createMock(WorkflowService.class);
    database = createNiceMock(ParticipationManagementDatabase.class);

    mediaPackage = MediaPackageImpl.valueOf(workflow);
    mediaPackage.setIdentifier(new IdImpl());

    workflowDefinition = new WorkflowDefinitionImpl();
    workflowDefinition.setId("0");

    workflowInstance = new WorkflowInstance();
    workflowInstance.setState(WorkflowInstance.WorkflowState.INSTANTIATED);
    workflowInstance.setId(0L);
    // workflowInstance.setMediaPackage(mediaPackage);
    workflowInstance.setOperations(new ArrayList<WorkflowOperationInstance>());
    workflowInstance.setConfiguration("key", "value");

    options = new HashMap<>();
    options.put("key", "value");

    SchedulingSource schedulingSource = new SchedulingSource("source");
    Room room = new Room("room");
    CaptureAgent captureAgent = new CaptureAgent(room, "0", "0");

    Person teacher = new Person(0L, "name", "email", new ArrayList<PersonType>());
    List<Person> personList = new ArrayList<>();
    personList.add(teacher);

    Course course = new Course("0");
    course.setId(0L);
    course.setSchedulingSource(Option.some(schedulingSource));

    Date dateTime = new Date();

    Recording recording = new Recording(
            Option.some(0L),
            "0",
            Option.some("0"),
            "value",
            personList,
            Option.some(course),
            room,
            dateTime,
            false,
            false,
            dateTime,
            dateTime,
            Course.EmailStatus.SENT,
            Recording.ReviewStatus.CONFIRMED,
            Option.some(new Date()),
            personList,
            new ArrayList<Message>(),
            captureAgent,
            new ArrayList<Action>(),
            Option.some("0"),
            false,
            "0"
    );
    recording.setSchedulingSource(Option.some(schedulingSource));
    recording.setCourse(Option.some(course));
    recording.setRoom(room);
    recording.setCaptureAgent(captureAgent);

    List<Recording> recordings = new ArrayList<>();
    recordings.add(recording);

    TeacherPm teacherPm = new TeacherPm();
    teacherPm.setParticipationManagementDatabase(database);
    teacherPm.setWorkflowService(workflowService);
    teacherPm.setEventCommentService(new EventCommentServiceImpl());
    teacherPm.setAssetServiceUtils(new AssetServiceUtils());
    WorkflowServiceUtils workflowServiceUtils = new WorkflowServiceUtils(workflowService);

    WorkflowInstance workflowInstance = new WorkflowInstance();
    workflowInstance.setId(0L);

    MediaPackage mediaPackage = MediaPackageImpl.valueOf(workflow);
    workflowInstance.setMediaPackage(mediaPackage);
    workflowInstance.setState(WorkflowInstance.WorkflowState.STOPPED);

    RecordingQuery query = RecordingQuery.create();
    query.withCourse(course)
            .withActivityId("0")
            .withRoom(room)
            .withStartDate(dateTime)
            .withoutDeleted()
            .withoutBlacklisted()
            .withEmailStatus(Course.EmailStatus.SENT)
            .withReviewStatus(Recording.ReviewStatus.CONFIRMED);

    teacherPm.setParticipationManagementDatabase(database);

    expect(database.findRecordings(anyObject(query.getClass()))).andReturn(recordings);
    expect(database.findRecordingsAsView(anyObject(RecordingQuery.create().getClass())))
            .andReturn(new ArrayList<>()).anyTimes();
    expect(workflowService.getWorkflowInstancesByMediaPackage("0"))
            .andReturn(new ArrayList<WorkflowInstance>()).anyTimes();
    replay(database);
    replay(workflowService);

    recordingContainer = new RecordingContainer(teacherPm, Option.some(workflowServiceUtils), teacher, false);
    assertEquals(1, recordingContainer.size());

    verify(database);
    verify(workflowService);
  }

  public void tearDown() throws Exception {
    super.tearDown();
  }

  public void testRefresh() {
    recordingContainer.refresh();
    assertEquals(0, recordingContainer.size());
  }
}
