/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.ui.teacher;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageImpl;
import org.opencastproject.mediapackage.identifier.IdImpl;
import org.opencastproject.security.api.UnauthorizedException;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowDefinitionImpl;
import org.opencastproject.workflow.api.WorkflowException;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowService;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

public class WorkflowServiceUtilsTest extends TestCase {
  private WorkflowServiceUtils workflowServiceUtils;

  private WorkflowService workflowService;
  private WorkflowDefinition workflowDefinition;
  private WorkflowInstance workflowInstance;
  private MediaPackage mediaPackage;
  private Map<String, String> options;

  public void setUp() throws Exception {
    super.setUp();

    String workflow = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
            .append("<definition xmlns=\"http://workflow.opencastproject.org\">")
            .append("</definition>")
            .toString();

    workflowService = createStrictMock(WorkflowService.class);
    workflowServiceUtils = new WorkflowServiceUtils(workflowService);

    mediaPackage = MediaPackageImpl.valueOf(workflow);
    mediaPackage.setIdentifier(new IdImpl());

    workflowDefinition = new WorkflowDefinitionImpl();
    workflowDefinition.setId("0");

    workflowInstance = new WorkflowInstance();
    workflowInstance.setState(WorkflowInstance.WorkflowState.INSTANTIATED);
    workflowInstance.setId(0L);
    // workflowInstance.setMediaPackage(mediaPackage);
    workflowInstance.setOperations(new ArrayList<WorkflowOperationInstance>());
    workflowInstance.setConfiguration("key", "value");

    options = new HashMap<>();
    options.put("key", "value");
  }

  public void tearDown() throws Exception {
  }

  /*public void testRunWorkflow() throws WorkflowDatabaseException, NotFoundException,
                                       WorkflowParsingException, UnauthorizedException {
    workflowInstance.setState(WorkflowInstance.WorkflowState.STOPPED);
    expect(workflowService.getWorkflowDefinitionById("0")).andStubReturn(workflowDefinition);
    expect(workflowService.start(workflowDefinition, mediaPackage)).andStubReturn(workflowInstance);
    replay(workflowService);

    Option<WorkflowInstance> wi = workflowServiceUtils.runWorkflow("0", mediaPackage);
    WorkflowInstance startedWorkflow = wi.get();
    assertEquals(0L, startedWorkflow.getId());
    verify(workflowService);
  }*/

  /*public void testStartWorkflow() throws WorkflowDatabaseException, NotFoundException,
                                         WorkflowParsingException, UnauthorizedException {
    expect(workflowService.getWorkflowDefinitionById("0")).andStubReturn(workflowDefinition);
    expect(workflowService.start(workflowDefinition, mediaPackage, options)).andStubReturn(workflowInstance);
    replay(workflowService);

    Option<WorkflowInstance> wi = workflowServiceUtils.startWorkflow("0", mediaPackage, options);
    WorkflowInstance startedWorkflow = wi.get();
    assertEquals(0L, startedWorkflow.getId());
    verify(workflowService);
  }*/

  public void testIsSucceeded() {
    workflowInstance.setState(WorkflowInstance.WorkflowState.SUCCEEDED);
    assertTrue(WorkflowServiceUtils.isSucceeded(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.STOPPED);
    assertFalse(WorkflowServiceUtils.isSucceeded(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.RUNNING);
    assertFalse(WorkflowServiceUtils.isSucceeded(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.INSTANTIATED);
    assertFalse(WorkflowServiceUtils.isSucceeded(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILING);
    assertFalse(WorkflowServiceUtils.isSucceeded(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.isSucceeded(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.isSucceeded(workflowInstance));
  }

  public void testIsPaused() {
    workflowInstance.setState(WorkflowInstance.WorkflowState.SUCCEEDED);
    assertFalse(WorkflowServiceUtils.isPaused(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.STOPPED);
    assertFalse(WorkflowServiceUtils.isPaused(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.RUNNING);
    assertFalse(WorkflowServiceUtils.isPaused(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.INSTANTIATED);
    assertTrue(WorkflowServiceUtils.isPaused(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILING);
    assertFalse(WorkflowServiceUtils.isPaused(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.isPaused(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.isPaused(workflowInstance));
  }

  public void testIsRunning() {
    workflowInstance.setState(WorkflowInstance.WorkflowState.SUCCEEDED);
    assertFalse(WorkflowServiceUtils.isRunning(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.STOPPED);
    assertFalse(WorkflowServiceUtils.isRunning(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.RUNNING);
    assertTrue(WorkflowServiceUtils.isRunning(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.INSTANTIATED);
    assertFalse(WorkflowServiceUtils.isRunning(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILING);
    assertFalse(WorkflowServiceUtils.isRunning(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.isRunning(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.isRunning(workflowInstance));
  }

  @Test
  public void testNotFinished() {
    workflowInstance.setState(WorkflowInstance.WorkflowState.SUCCEEDED);
    assertFalse(WorkflowServiceUtils.notFinished(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.STOPPED);
    assertFalse(WorkflowServiceUtils.notFinished(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.RUNNING);
    assertTrue(WorkflowServiceUtils.notFinished(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.INSTANTIATED);
    assertTrue(WorkflowServiceUtils.notFinished(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILING);
    assertTrue(WorkflowServiceUtils.notFinished(workflowInstance));

    workflowInstance.setState(WorkflowInstance.WorkflowState.FAILED);
    assertFalse(WorkflowServiceUtils.notFinished(workflowInstance));
  }

  public void testGetSvc() {
    assertEquals(workflowService, workflowServiceUtils.getSvc());
  }

  public void testPauseWorkflow() throws WorkflowException, UnauthorizedException, NotFoundException {
    expect(workflowService.suspend(0L)).andStubReturn(workflowInstance);
    replay(workflowService);

    assertTrue(workflowServiceUtils.pauseWorkflow(0L));

    verify(workflowService);
  }

  public void testPauseWorkflowNotFound() throws WorkflowException, UnauthorizedException, NotFoundException {
    expect(workflowService.suspend(0L)).andThrow(new NotFoundException());
    replay(workflowService);

    assertFalse(workflowServiceUtils.pauseWorkflow(0L));

    verify(workflowService);
  }
}
