/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.syllabus.remote.endpoint;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.opencastproject.pm.syllabus.api.Activities;
import org.opencastproject.pm.syllabus.api.SyllabusDataService;
import org.opencastproject.pm.syllabus.api.VActivityDateTime;
import org.opencastproject.pm.syllabus.api.VActivityLocation;
import org.opencastproject.pm.syllabus.api.VLocation;
import org.opencastproject.pm.syllabus.api.VModule;
import org.opencastproject.pm.syllabus.api.VStaff;
import org.opencastproject.pm.syllabus.impl.ActivitiesImpl;
import org.opencastproject.pm.syllabus.impl.RemoteObjectUtil;
import org.opencastproject.pm.syllabus.impl.SyllabusDataImpl;
import org.opencastproject.pm.syllabus.impl.VLocationImpl;
import org.opencastproject.pm.syllabus.impl.security.RemoteAccessHttpsClient;
import org.opencastproject.util.data.Option;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RemoteObjectUtil.class)
public class SyllabusDataRestServiceTest {
  private SyllabusDataRestService syllabusDataRestService;
  private SyllabusDataService syllabusDataService;
  private RemoteAccessHttpsClient remoteAccessHttpsClient;

  @Before
  public void setUp() {
    syllabusDataService = createNiceMock(SyllabusDataService.class);
    remoteAccessHttpsClient = createNiceMock(RemoteAccessHttpsClient.class);

    syllabusDataRestService = new SyllabusDataRestService();
    syllabusDataRestService.setSyllabusDataService(syllabusDataService);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testFetchEmpty() {
    Response response = syllabusDataRestService.fetch("");
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFetchNull() {
    Response response = syllabusDataRestService.fetch(null);
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFetchModules() {
    expect(syllabusDataService.fetchModules()).andStubReturn(new SyllabusDataImpl());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.fetch("modules");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFetchOther() {
    Response response = syllabusDataRestService.fetch("other");
    assertEquals(400, response.getStatus());
  }

  @Test
  public void testGetSourceDescription() {
    replay(syllabusDataService);

    expect(remoteAccessHttpsClient.getRemoteBaseAddress()).andStubReturn("http://localhost");
    replay(remoteAccessHttpsClient);

    Response response = syllabusDataRestService.getSourceDescription();
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
    verify(remoteAccessHttpsClient);
  }

  @Test
  public void testGetModuleByCourseKey404() {
    Response response = syllabusDataRestService.getModuleByCourseKey("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testGetModuleByCourseKey200() {
    expect(syllabusDataService.getModuleByCourseKey("")).andStubReturn(createNiceMock(VModule.class));
    replay(syllabusDataService);

    Response response = syllabusDataRestService.getModuleByCourseKey("");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindModuleActivityIdsByCourseKey() {
    Response response = syllabusDataRestService.findModuleActivityIdsByCourseKey("");
    assertEquals(200, response.getStatus());
  }

  @Test
  public void testGetAllModules404() {
    Response response = syllabusDataRestService.getAllModule();
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testGetAllModules200() {
    expect(syllabusDataService.getAllModule()).andStubReturn(new ArrayList<VModule>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.getAllModule();
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityDateTimeByRangeEmpty() {
    Response response = syllabusDataRestService.findActivityDateTimeByRange("", "");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindActivityDateTimeByRangeNull() {
    Response response = syllabusDataRestService.findActivityDateTimeByRange(null, null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindActivityDateTimeByRangeEmptyList() {
    expect(syllabusDataService.findActivityDateTimeByRange("value", "value"))
        .andStubReturn(new ArrayList<VActivityDateTime>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityDateTimeByRange("value", "value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityDateTimeByRange200() {
    List<VActivityDateTime> activities = new ArrayList<>();
    activities.add(createNiceMock(VActivityDateTime.class));

    expect(syllabusDataService.findActivityDateTimeByRange("value", "value")).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityDateTimeByRange("value", "value");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameterEmpty() {
    Response response = syllabusDataRestService.findActivityByParameter("", "", "", "", "");
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFindActivityByParameterNull() {
    Response response = syllabusDataRestService.findActivityByParameter(null, null, null, null, null);
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFindActivityByParameterModuleName() throws JsonProcessingException {
    List<Activities> activities = new ArrayList<>();
    activities.add(new ActivitiesImpl("",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      false,
                                      false,
                                      false,
                                      false,
                                      "",
                                      new DateTime(),
                                      new DateTime()));

    PowerMock.mockStatic(RemoteObjectUtil.class);
    expect(RemoteObjectUtil.writeJson(activities)).andReturn(Response.ok(activities).build());
    PowerMock.replay(RemoteObjectUtil.class);
    expect(syllabusDataService.findActivityByModule("%value%")).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("value", "", "", "", "");
    assertEquals(200, response.getStatus());

    PowerMock.verify(RemoteObjectUtil.class);
    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameterStaffId() throws JsonProcessingException {
    List<Activities> activities = new ArrayList<>();
    activities.add(new ActivitiesImpl("",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      false,
                                      false,
                                      false,
                                      false,
                                      "",
                                      new DateTime(),
                                      new DateTime()));

    PowerMock.mockStatic(RemoteObjectUtil.class);
    expect(RemoteObjectUtil.writeJson(activities)).andReturn(Response.ok(activities).build());
    PowerMock.replay(RemoteObjectUtil.class);
    expect(syllabusDataService.findActivityByStaffId("value")).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("", "value", "", "", "");
    assertEquals(200, response.getStatus());

    PowerMock.verify(RemoteObjectUtil.class);
    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameterLocationId() throws JsonProcessingException {
    List<Activities> activities = new ArrayList<>();
    activities.add(new ActivitiesImpl("",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      false,
                                      false,
                                      false,
                                      false,
                                      "",
                                      new DateTime(),
                                      new DateTime()));

    DateTime start = new DateTime().withDate(1970, 1, 1).withTime(0, 0, 0, 0);
    DateTime end = new DateTime().withDate(1970, 1, 1).withTime(0, 0, 0, 0);

    PowerMock.mockStatic(RemoteObjectUtil.class);
    expect(RemoteObjectUtil.writeJson(activities)).andReturn(Response.ok(activities).build());
    PowerMock.replay(RemoteObjectUtil.class);
    expect(syllabusDataService.findActivitiesByLocation("value", start, end)).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("", "", "value", "1970-01-01", "1970-01-01");
    assertEquals(200, response.getStatus());

    PowerMock.verify(RemoteObjectUtil.class);
    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameterWithoutParameter() {
    List<Activities> activities = new ArrayList<>();
    activities.add(new ActivitiesImpl("",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      false,
                                      false,
                                      false,
                                      false,
                                      "",
                                      new DateTime(),
                                      new DateTime()));

    expect(syllabusDataService.findActivityByModule("%value%")).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("", "", "", "", "");
    assertEquals(500, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameterInvalidDate() {
    List<Activities> activities = new ArrayList<>();
    activities.add(new ActivitiesImpl("",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      false,
                                      false,
                                      false,
                                      false,
                                      "",
                                      new DateTime(),
                                      new DateTime()));

    DateTime start = new DateTime();
    DateTime end = new DateTime();

    expect(syllabusDataService.findActivitiesByLocation("value", start, end)).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("", "", "value", "value", "value");
    assertEquals(500, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameterEmptyList() {
    DateTime start = new DateTime().withDate(1970, 1, 1).withTime(0, 0, 0, 0);
    DateTime end = new DateTime().withDate(1970, 1, 1).withTime(0, 0, 0, 0);

    expect(syllabusDataService.findActivitiesByLocation("value", start, end))
        .andStubReturn(new ArrayList<Activities>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("", "", "value", "1970-01-01", "1970-01-01");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityByParameter200() throws JsonProcessingException {
    DateTime start = new DateTime().withDate(1970, 1, 1).withTime(0, 0, 0, 0);
    DateTime end = new DateTime().withDate(1970, 1, 1).withTime(0, 0, 0, 0);

    List<Activities> activities = new ArrayList<>();
    activities.add(new ActivitiesImpl("value",
                                      "value",
                                      "value",
                                      "value",
                                      "value",
                                      "value",
                                      false,
                                      false,
                                      false,
                                      false,
                                      "",
                                      start,
                                      end));

    PowerMock.mockStatic(RemoteObjectUtil.class);
    expect(RemoteObjectUtil.writeJson(activities)).andReturn(Response.ok(activities).build());
    PowerMock.replay(RemoteObjectUtil.class);
    expect(syllabusDataService.findActivitiesByLocation("value", start, end)).andStubReturn(activities);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityByParameter("", "", "value", "1970-01-01", "1970-01-01");
    assertEquals(200, response.getStatus());

    PowerMock.verify(RemoteObjectUtil.class);
    verify(syllabusDataService);
  }

  @Test
  public void testFindChildActivityEmpty() {
    Response response = syllabusDataRestService.findChildActivity("");
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFindChildActivityNull() {
    Response response = syllabusDataRestService.findChildActivity(null);
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFindChildActivityEmptyList() {
    expect(syllabusDataService.findChildActivity("value")).andStubReturn(new ArrayList<Activities>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findChildActivity("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindChildActivity200() {
    Activities activity = createNiceMock(Activities.class);
    List<Activities> activities = new ArrayList<>();
    activities.add(activity);

    expect(syllabusDataService.findChildActivity("value")).andStubReturn(activities);
    replay(activity);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findChildActivity("value");
    assertEquals(200, response.getStatus());

    verify(activity);
    verify(syllabusDataService);
  }

  @Test
  public void testFindModuleByIdEmpty() {
    Response response = syllabusDataRestService.findModuleById("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindModuleByIdNull() {
    Response response = syllabusDataRestService.findModuleById(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindModuleById404() {
    expect(syllabusDataService.getModuleById("value")).andStubReturn(null);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findModuleById("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindModuleById200() throws JsonProcessingException {
    VModule module = createNiceMock(VModule.class);
    ObjectMapper mapper = createNiceMock(ObjectMapper.class);

    expect(mapper.writeValueAsString(module)).andStubReturn("");
    expect(syllabusDataService.getModuleById("value")).andStubReturn(module);

    replay(module);
    replay(mapper);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findModuleById("value");
    assertEquals(200, response.getStatus());

    verify(module);
    verify(mapper);
    verify(syllabusDataService);
  }

  @Test
  public void testFindStaffIdEmpty() {
    Response response = syllabusDataRestService.findStaffById("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindStaffByIdNull() {
    Response response = syllabusDataRestService.findStaffById(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindStaffById404() {
    expect(syllabusDataService.findStaffById("value")).andStubReturn(null);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findStaffById("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindStaffById200() {
    VStaff staff = createNiceMock(VStaff.class);

    expect(syllabusDataService.findStaffById("value")).andStubReturn(staff);

    replay(staff);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findStaffById("value");
    assertEquals(200, response.getStatus());

    verify(staff);
    verify(syllabusDataService);
  }

  @Test
  public void testFindStaffByStaffActivityIdEmpty() {
    Response response = syllabusDataRestService.findStaffByStaffActivityId("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindStaffByStaffActivityIdNull() {
    Response response = syllabusDataRestService.findStaffByStaffActivityId(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindStaffByStaffActivityId404() {
    expect(syllabusDataService.findStaffByStaffActivityId("value")).andStubReturn(null);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findStaffByStaffActivityId("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindStaffByStaffActivityId200() {
    expect(syllabusDataService.findStaffByStaffActivityId("value")).andStubReturn(new ArrayList<VStaff>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findStaffByStaffActivityId("value");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindByActivityLocationIdEmpty() {
    Response response = syllabusDataRestService.findByActivityLocationId("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindByActivityLocationIdNull() {
    Response response = syllabusDataRestService.findByActivityLocationId(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindByActivityLocationId404() {
    expect(syllabusDataService.findByActivityLocationId("value")).andStubReturn(null);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findByActivityLocationId("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindByActivityLocationIdEmptyList() {
    expect(syllabusDataService.findByActivityLocationId("value")).andStubReturn(new ArrayList<VActivityLocation>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findByActivityLocationId("value");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindByActivityLocationId200() {
    VActivityLocation activityLocation = createNiceMock(VActivityLocation.class);
    List<VActivityLocation> activities = new ArrayList<>();
    activities.add(activityLocation);

    expect(syllabusDataService.findByActivityLocationId("value")).andStubReturn(activities);
    replay(activityLocation);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findByActivityLocationId("value");
    assertEquals(200, response.getStatus());

    verify(activityLocation);
    verify(syllabusDataService);
  }

  @Test
  public void testFindLocationByNameEmpty() {
    Response response = syllabusDataRestService.findLocationByName("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindLocationByNameNull() {
    Response response = syllabusDataRestService.findLocationByName(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindLocationByName404() {
    expect(syllabusDataService.findLocationByName("value")).andStubReturn(null);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findLocationByName("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindLocationByName200() throws JsonProcessingException {
    VLocation location = new VLocationImpl("", "", "", new DateTime(), Option.some(""));

    PowerMock.mockStatic(RemoteObjectUtil.class);
    expect(RemoteObjectUtil.writeJson(location)).andReturn(Response.ok(location).build());
    PowerMock.replay(RemoteObjectUtil.class);
    expect(syllabusDataService.findLocationByName("value")).andStubReturn(location);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findLocationByName("value");
    assertEquals(200, response.getStatus());

    PowerMock.verify(RemoteObjectUtil.class);
    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityDateTimeEmpty() {
    Response response = syllabusDataRestService.findActivityDateTime("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindActivityDateTimeNull() {
    Response response = syllabusDataRestService.findActivityDateTime(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindActivityDateTime404() {
    expect(syllabusDataService.findActivityDateTime("value")).andStubReturn(null);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityDateTime("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityDateTimeEmptyList() {
    expect(syllabusDataService.findActivityDateTime("value")).andStubReturn(new ArrayList<VActivityDateTime>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityDateTime("value");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindActivityDateTime200() {
    VActivityDateTime activityDateTime = createNiceMock(VActivityDateTime.class);

    List<VActivityDateTime> activities = new ArrayList<>();
    activities.add(activityDateTime);

    expect(syllabusDataService.findActivityDateTime("value")).andStubReturn(activities);
    replay(activityDateTime);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findActivityDateTime("value");
    assertEquals(200, response.getStatus());

    verify(activityDateTime);
    verify(syllabusDataService);
  }

  @Test
  public void testFindSuitabilityByLocationidEmpty() {
    Response response = syllabusDataRestService.findSuitabilityByLocationid("");
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindSuitabilityByLocationidNull() {
    Response response = syllabusDataRestService.findSuitabilityByLocationid(null);
    assertEquals(404, response.getStatus());
  }

  @Test
  public void testFindSuitabilityByLocationid200() {
    expect(syllabusDataService.findSuitabilityByLocationId("value")).andStubReturn(new ArrayList<VLocation>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findSuitabilityByLocationid("value");
    assertEquals(200, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindParentActivitiesEmpty() {
    Response response = syllabusDataRestService.findParentActivities("");
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFindParentActivitiesNull() {
    Response response = syllabusDataRestService.findParentActivities(null);
    assertEquals(500, response.getStatus());
  }

  @Test
  public void testFindParentActivities404() {
    expect(syllabusDataService.findParentActivities("value")).andStubReturn(new ArrayList<Activities>());
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findParentActivities("value");
    assertEquals(404, response.getStatus());

    verify(syllabusDataService);
  }

  @Test
  public void testFindParentActivities200() {
    Activities activity = createNiceMock(Activities.class);
    List<Activities> activities = new ArrayList<>();
    activities.add(activity);

    expect(syllabusDataService.findParentActivities("value")).andStubReturn(activities);
    replay(activity);
    replay(syllabusDataService);

    Response response = syllabusDataRestService.findParentActivities("value");
    assertEquals(200, response.getStatus());

    verify(activity);
    verify(syllabusDataService);
  }
}
