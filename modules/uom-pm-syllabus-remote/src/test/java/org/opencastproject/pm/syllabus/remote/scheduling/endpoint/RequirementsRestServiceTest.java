/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package org.opencastproject.pm.syllabus.remote.scheduling.endpoint;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;
import uk.ac.manchester.requirement.RequirementService;
import uk.ac.manchester.requirement.RequirementServiceException;

public class RequirementsRestServiceTest extends TestCase {
  private RequirementsRestService requirementsRestService;
  private RequirementService requirementService;

  public void setUp() throws Exception {
    super.setUp();

    requirementsRestService = new RequirementsRestService();
    requirementService = createNiceMock(RequirementService.class);
  }

  public void tearDown() {
  }

  public void testGetResources503() {
    Response response = requirementsRestService.getResources("", "", "", "");
    assertEquals(503, response.getStatus());
  }

  public void testGetResourcesEmpty() {
    requirementsRestService.addRequirementService(requirementService);

    Response response = requirementsRestService.getResources("dass", "", "", "");
    assertEquals(400, response.getStatus());
  }

  public void testGetResources404() throws RequirementServiceException {
    expect(requirementService.checkId("value", RequirementService.Resource.USER,
            RequirementService.Requirement.RECORDING)).andStubReturn(false);
    replay(requirementService);

    requirementsRestService.addRequirementService(requirementService);

    Response response = requirementsRestService.getResources("dass", "user", "recording", "value");
    assertEquals(404, response.getStatus());

    verify(requirementService);
  }

  public void testGetResourcesIdEmpty() throws RequirementServiceException {
    expect(requirementService.checkId("value", RequirementService.Resource.USER,
            RequirementService.Requirement.RECORDING)).andStubReturn(true);
    replay(requirementService);

    requirementsRestService.addRequirementService(requirementService);

    Response response = requirementsRestService.getResources("dass", "user", "recording", "");
    assertEquals(200, response.getStatus());

    verify(requirementService);
  }

  public void testGetResources200() throws RequirementServiceException {
    expect(requirementService.checkId("value", RequirementService.Resource.USER,
            RequirementService.Requirement.RECORDING)).andStubReturn(true);
    replay(requirementService);

    requirementsRestService.addRequirementService(requirementService);

    Response response = requirementsRestService.getResources("dass", "user", "recording", "value");
    assertEquals(200, response.getStatus());

    verify(requirementService);
  }
}
