/**
 * Licensed to The Apereo Foundation under one or more contributor license
 * agreements. See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 *
 * The Apereo Foundation licenses this file to you under the Educational
 * Community License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License
 * at:
 *
 *   http://opensource.org/licenses/ecl2.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.opencastproject.pm.ui.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.opencastproject.util.data.Effect;
import org.opencastproject.util.data.Function;
import org.opencastproject.util.data.Tuple;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import org.junit.Test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ListResourceBundle;

public class UiUtilTest {
  @Test
  public void testTimeFormat() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
    Date date = calendar.getTime();

    DateFormat timeFormat = UiUtil.timeFormat();
    assertEquals("00:00", timeFormat.format(date));
  }

  @Test
  public void testDateFormat() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
    Date date = calendar.getTime();

    DateFormat dateFormat = UiUtil.dateFormat();
    assertEquals("1970-01-01", dateFormat.format(date));
  }

  @Test
  public void testDateTimeFormat() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
    Date date = calendar.getTime();

    DateFormat dateTimeFormat = UiUtil.dateTimeFormat();
    assertEquals("1970-01-01 00:00", dateTimeFormat.format(date));
  }

  @Test
  public void testDateTimeFormatSecond() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
    Date date = calendar.getTime();

    DateFormat dateTimeFormat = UiUtil.dateTimeFormatSecond();
    assertEquals("1970-01-01 00:00:00", dateTimeFormat.format(date));
  }

  @Test
  public void testTableDateColGen() {
    Table.ColumnGenerator result = UiUtil.tableDateColGen(new Function<Object, Date>() {
      @Override
      public Date apply(Object o) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
        return calendar.getTime();
      }
    });
    Label expected = new Label("1970-01-01");
    Label actual = (Label) result.generateCell(new Table(), "0", "0");
    assertEquals(expected.getValue(), actual.getValue());
  }

  @Test
  public void testTableStringColGen() {
    Table.ColumnGenerator result = UiUtil.tableStringColGen(new Function<Object, String>() {
      @Override
      public String apply(Object o) {
        return "";
      }
    });
    assertEquals("", result.generateCell(new Table(), "", ""));
  }

  @Test
  public void testI18n() {
    I18N result = UiUtil.i18n(new ListResourceBundle() {
      @Override
      protected Object[][] getContents() {
        return new Object[0][];
      }
    });
    assertEquals("", result.s(""));
    assertEquals("value", result.s("value"));
  }

  @Test
  public void testJoin() {
    assertEquals("1 2 3 4", UiUtil.join("1", "2", "3", "4"));
  }

  @Test
  public void testTabs() {
    TabSheet tabSheet = UiUtil.tabs(new Tuple<String, ComponentContainer>("", new HorizontalLayout()));
    assertEquals(0, tabSheet.getTabIndex());
  }

  @Test
  public void testVlayout() {
    VerticalLayout result = UiUtil.vlayout();
    result.addComponent(new Label());
    assertEquals(1, result.getComponentCount());

    result = UiUtil.vlayout(new Effect<AbstractOrderedLayout>() {
      @Override
      protected void run(AbstractOrderedLayout components) {
        components.addComponent(new Label());
      }
    });
    assertEquals(1, result.getComponentCount());
  }

  @Test
  public void testHlayout() {
    HorizontalLayout result = UiUtil.hlayout();
    result.addComponent(new Label());
    assertEquals(1, result.getComponentCount());

    result = UiUtil.hlayout(new Effect<AbstractOrderedLayout>() {
      @Override
      protected void run(AbstractOrderedLayout components) {
        components.addComponent(new Label());
      }
    });
    assertEquals(1, result.getComponentCount());
  }

  @Test
  public void testLabel() {
    Label result = UiUtil.label("", new Effect<Label>() {
      @Override
      public void run(Label label) {
        label.setValue("value");
      }
    });
    assertEquals("value", result.getValue());
  }

  @Test
  public void testVspacer() {
    Label result = UiUtil.vspacer();
    assertEquals(" ", result.getValue());
  }

  @Test
  public void testHspacer() {
    Label result = UiUtil.hspacer();
    assertEquals(" ", result.getValue());
  }

  @Test
  public void testVspacerBig() {
    Label result = UiUtil.vspacerBig();
    assertEquals(" ", result.getValue());
  }

  @Test
  public void testByteSize() {
    Function<Long, String> result = UiUtil.byteSize();
    assertEquals("-1.000000", result.apply(-1L));
    assertEquals("0.000000", result.apply(0L));
    assertEquals("1.0 KB", result.apply(1024L));
    assertEquals("1.0 MB", result.apply(1048576L));
    assertEquals("1.0 GB", result.apply(1073741824L));
    assertEquals("1.0 TB", result.apply(1099511628000L));
  }

  @Test
  public void testMarginsFalse() {
    MarginInfo marginInfo = UiUtil.margins(false, false, false, false);
    assertFalse(marginInfo.hasTop());
    assertFalse(marginInfo.hasRight());
    assertFalse(marginInfo.hasBottom());
    assertFalse(marginInfo.hasLeft());
    assertEquals(0, marginInfo.getBitMask());
  }

  @Test
  public void testMarginsTrue() {
    MarginInfo marginInfo = UiUtil.margins(true, true, true, true);
    assertTrue(marginInfo.hasTop());
    assertTrue(marginInfo.hasRight());
    assertTrue(marginInfo.hasBottom());
    assertTrue(marginInfo.hasLeft());
    assertEquals(15, marginInfo.getBitMask());
  }

  @Test
  public void testEnable() {
    Label label = new Label();
    UiUtil.enable.apply(label);
    assertTrue(label.isEnabled());
  }

  @Test
  public void testSizeFull() {
    Label label = new Label();
    UiUtil.sizeFull.apply(label);
    assertEquals(100.0, label.getWidth(), 0);
    assertEquals(100.0, label.getHeight(), 0);
  }

  @Test
  public void testBold() {
    Label label = new Label();
    UiUtil.bold.apply(label);
    assertEquals("bold", label.getStyleName());
  }

  @Test
  public void testPageTitle() {
    Label label = new Label();
    UiUtil.pageTitle.apply(label);
    assertEquals("page-title", label.getStyleName());
  }

  @Test
  public void testPageSubTitle() {
    Label label = new Label();
    UiUtil.pageSubTitle.apply(label);
    assertEquals("page-sub-title", label.getStyleName());
  }

  @Test
  public void testSizeUndefined() {
    Label label = new Label();
    label.setValue("title");
    UiUtil.sizeUndefined.apply(label);
    assertEquals("title", label.getValue());
  }

  @Test
  public void testHtmlLabel() {
    Label label = new Label();
    UiUtil.htmlLabel.apply(label);
    assertEquals(ContentMode.HTML, label.getContentMode());
  }

  @Test
  public void testWithMarginWithMarginHandler() {
    Layout.MarginHandler marginHandler = new HorizontalLayout();
    UiUtil.withMargin.apply(marginHandler);
    MarginInfo marginInfo = new MarginInfo(true);
    assertEquals(marginInfo, marginHandler.getMargin());
  }

  @Test
  public void testWithoutMargin() {
    Layout.MarginHandler marginHandler = new HorizontalLayout();
    UiUtil.withoutMargin.apply(marginHandler);
    MarginInfo marginInfo = marginHandler.getMargin();
    assertFalse(marginInfo.hasTop());
    assertFalse(marginInfo.hasRight());
    assertFalse(marginInfo.hasBottom());
    assertFalse(marginInfo.hasLeft());
    assertEquals(0, marginInfo.getBitMask());
  }

  @Test
  public void testTopLeft() {
    Label label = new Label();
    label.setValue("title");
    UiUtil.topLeft.apply(label);
  }

  @Test
  public void testSpacing() {
    Label label = new Label();
    label.setValue("title");
    UiUtil.spacing.apply(new HorizontalLayout());
    assertEquals((float) 100.0, label.getWidth(), 0);
    assertEquals((float) -1.0, label.getHeight(), 0);
  }
}
