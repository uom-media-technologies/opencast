// Build & Package Opencast
pipeline {
    agent any

    triggers {
        pollSCM 'H/5 * * * *'
    }

    tools {
        // Required tools must match Jenkins Server's labels
        maven "Maven 3"
        jdk "OpenJDK 11"
    }

    environment {
        PRODUCT = "Opencast"
        VERSION = "12.13"
        GIT_CREDS = 'jenkins-bitbucket'
    }

    stages {
        stage('Build') {
            steps {
                checkout(
                    $class: 'GitSCM',
                    branches: [[name: '*/$REVISION']],
                    extensions: [],
                    userRemoteConfigs: [[credentialsId: 'jenkins-bitbucket', url: 'git@bitbucket.org:uom-media-technologies/opencast.git']]
                )
                script {
                    env.GIT_AUTHOR = sh (script: 'git log -1 --pretty=%cn ${GIT_COMMIT}', returnStdout: true).trim()
                }
                slackSend (
                    color: 'good',
                    message: "$JOB_NAME #$BUILD_NUMBER Started by changes from $GIT_AUTHOR (<$BUILD_URL|Open>)"
                )
                sh 'mvn -Dmaven.test.failure.ignore=true -Pcomplete -DskipTests=$SKIP_TESTS clean install'
            }

            post {
                always {
                    script {
                        if (!env.SKIP_TESTS) {
                            junit '**/target/surefire-reports/TEST-*.xml'
                        }
                    }
                }
            }
        }

        stage('Package') {
            environment {
                RPM_REPOSITORY_ROOT = '/var/www/rpm-repos'
            }

            steps {
                sh 'sudo $WORKSPACE/docs/scripts/rpm/rpm-build.py -w $WORKSPACE -a "opencast-dist-complete" -p manchester -e $TARGET -d $RPM_REPOSITORY_ROOT'
            }
        }

        stage('Tag Release') {
            when {
               expression { "$TARGET" == 'production' }
            }

            steps {
                script {
                    DATETIME = sh(script: "date +'%Y%m%d.%H%M%S'", returnStdout: true )
                }
                sshagent( credentials: ["$GIT_CREDS"] ) {
                    sh """
                        git tag RELEASE-$VERSION-$DATETIME
                        git push --tags
                    """
                }
            }
        }
    }

    post {
        success {
            slackSend (
                color: 'good',
                message: "$JOB_NAME #$BUILD_NUMBER Succeeded (<$BUILD_URL|Open>)"
            )
        }
        unstable {
            slackSend (
                color: 'warning',
                message: "$JOB_NAME #$BUILD_NUMBER Unstable (<$BUILD_URL|Open>)"
            )
        }
        failure {
            slackSend (
                color: 'danger',
                message: "$JOB_NAME #$BUILD_NUMBER Failed (<$BUILD_URL|Open>)"
            )
        }

        // regain diskpace
        cleanup {
            sh 'rm -rf $WORKSPACE/build'
            sh 'mvn clean'
        }
    }
}
